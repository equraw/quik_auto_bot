﻿using System;
using QuikSharp;
using QuikSharp.DataStructures;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuikSharp.DataStructures.Transaction;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;


namespace QuikAutoBot
{
    class Commands
    {
        public static readonly string[,] CList = new string[,] {
            { "Получить исторические данные" ,
                "Получить и отобразить исторические данные котировок по заданному инструменту." },

            { "Выставить заявку",
                "Будет выставлена заявку на покупку 1-го лота заданного инструмента, по цене"+
                " на 5 шагов цены выше текущей цены (вероятность срабатывания такой заявки достаточно высокая!!!)" },

            { "Удалить активную заявку",
                "Если предварительно была выставлена заявка, заявка имеет статус 'Активна' и"+
                " ее номер отображается в форме, то эта заявка будет удалена/отменена" },

            { "Получить таблицу лимитов по бумаге",
                "Получить и отобразить таблицу лимитов по бумагам." },

            { "Получить таблицу лимитов по всем бумагам",
                "Получить и отобразить таблицу лимитов по бумагам." },

            { "Получить таблицу заявок",
                "Получить и отобразить таблицу всех клиентских заявок." },

            { "Получить таблицу сделок",
                "Получить и отобразить таблицу всех клиентских сделок." },

            { "Получить таблицу `Клиентский портфель`",
            "Получить и отобразить таблицу `Клиентский портфель`."},

            { "Получить таблицы денежных лимитов",
                "Получить и отобразить таблицы денежных лимитов (стандартную и дополнительную Т2). Работает только на инструментах"+
                " фондовой секции." }
        };
        public InvokePrint Print { get; set; }
        public Tool tool { get; set; }

        /// <summary>
        /// Получить исторические данные
        /// </summary>
        public void GetHistData(CandleInterval interval)
        {
            Print("Получаем исторические данные...");
            List<Candle> toolCandles = QuikInstance.quik.Candles.GetAllCandles(tool.ClassCode, tool.SecurityCode, interval).Result;
            Print("Данные получено.");
            Application.Run(new FormOutputTable(toolCandles));
        }

        /// <summary>
        /// Выставить заявку 
        /// </summary>
        /// <returns></returns>
        public Order SubmitOrderAE()
        {
            decimal priceInOrder = Math.Round(tool.LastPrice + tool.Step * 5, tool.PriceAccuracy);
            return tool.SubmitOrder(Operation.Buy, priceInOrder, 1);
        }

        /// <summary>
        /// Удалить активную заявку
        /// </summary>
        public void DelActiveTicket(Order order)
        {
            if (order != null && order.OrderNum > 0)
            {
                Print("Удаляем заявку на покупку с номером: " + order.OrderNum + " ...");
            }
            long x = QuikInstance.quik.Orders.KillOrder(order).Result;
            Print("Результат: " + x);
        }

        /// <summary>
        /// Получить таблицу лимитов по бумаге из таблицы 'Лимиты по бумагам'
        /// </summary>
        public void GeLlimitsOnPaper()
        {
            Print("Получаем таблицу лимитов...");
            List<DepoLimitEx> listDepoLimits = QuikInstance.quik.Trading.GetDepoLimits(tool.SecurityCode).Result;
            if (listDepoLimits.Count > 0)
            {
                Print("Данные получено.");
                Application.Run(new FormOutputTable(listDepoLimits));
            }
            else
            {
                Print("Бумага '" + tool.Name + "' в таблице лимитов отсутствует.");
            }
        }

        /// <summary>
        /// Получить таблицу лимитов по всем бумагам из таблицы 'Лимиты по бумагам'
        /// </summary>
        public void GeLlimitsOnPaperAll()
        {
            Print("Получаем таблицу лимитов...");
            List<DepoLimitEx> listDepoLimits = QuikInstance.quik.Trading.GetDepoLimits().Result;

            if (listDepoLimits.Count > 0)
            {
                Print("Данные получено.");
                Application.Run(new FormOutputTable(listDepoLimits));
            }
            else
            {
                Print("Список записей пуст.");
            }
        }

        /// <summary>
        /// Получить таблицу заявок
        /// </summary>
        public void GetTableRequests()
        {
            Print("Получаем таблицу заявок...");
            List<Order> listOrders = QuikInstance.quik.Orders.GetOrders().Result;

            if (listOrders.Count > 0)
            {
                Print("Данные получено.");
                Application.Run(new FormOutputTable(listOrders));
            }
            else
            {
                Print("В списке нету заявок.");
            }
        }

        /// <summary>
        /// Получить таблицу сделок
        /// </summary>
        public void GetTableTransactions()
        {
            Print("Получаем таблицу сделок...");
            List<Trade> listTrades = QuikInstance.quik.Trading.GetTrades().Result;

            if (listTrades.Count > 0)
            {
                Print("Данные получено.");
                Application.Run(new FormOutputTable(listTrades));
            }
            else
            {
                Print("Таблица сделок пуста.");
            }
        }

        /// <summary>
        /// Получить таблицу `Клиентский портфель`
        /// </summary>
        public void GetClientPortfolio(string clientCode, string classCode)
        {
            Print("Получаем таблицу `Клиентский портфель`...");
            List<PortfolioInfoEx> listPortfolio = new List<PortfolioInfoEx>();
            if (classCode == "SPBFUT") listPortfolio.Add(QuikInstance.quik.Trading.GetPortfolioInfoEx(tool.FirmID, tool.AccountID, 0).Result);
            else listPortfolio.Add(QuikInstance.quik.Trading.GetPortfolioInfoEx(tool.FirmID, clientCode, 2).Result);

            if (listPortfolio.Count > 0)
            {
                Print("Данные получено.");
                Application.Run(new FormOutputTable(listPortfolio));
            }
            else
            {
                Print("В таблице 'Клиентский портфель' отсутствуют записи.");
            }
        }

        /// <summary>
        /// Получить таблицы денежных лимитов
        /// </summary>
        public void GetCashLimits(string clientCode)
        {
            Print("Получаем таблицу денежных лимитов...");
            List<MoneyLimit> listMoneyLimits = new List<MoneyLimit>();
            listMoneyLimits.Add(QuikInstance.quik.Trading.GetMoney(clientCode, tool.FirmID, "EQTV", "SUR").Result);
            if (listMoneyLimits.Count > 0)
            {
                Print("Данные получено.");
                ThreadPool.QueueUserWorkItem((x) => Application.Run(new FormOutputTable(listMoneyLimits)));
            }
            else
            {
                Print("Информации по лимитам нету.");
            }

            Print("Получаем расширение таблицы денежных лимитов...");
            List<MoneyLimitEx> listMoneyLimitsEx = new List<MoneyLimitEx>();
            listMoneyLimitsEx.Add(QuikInstance.quik.Trading.GetMoneyEx(tool.FirmID, clientCode, "EQTV", "SUR", 2).Result);
            if (listMoneyLimitsEx.Count > 0)
            {
                Print("Расширеные данные получено.");
                ThreadPool.QueueUserWorkItem((x) => Application.Run(new FormOutputTable(listMoneyLimitsEx)));
            }
            else
            {
                Print("Информации по расширеным лимитам нету.");
            }
        }
    }
}
