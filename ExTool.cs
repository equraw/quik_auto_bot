﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuikAutoBot
{
    public static class ExTool
    {
        public class StopOrder
        {
            public bool DirectionBuy { get; protected set; } //направление по котороу ордер сработает
            public decimal Amount { get; protected set; }  //сума
            public decimal StopPrice { get; protected set; }  //цена срабатывания стоп-ордера

            public StopOrder(bool DirectionBuy, decimal Amount, decimal StopPrice)
            {
                this.DirectionBuy = DirectionBuy;
                this.Amount = Amount;
                this.StopPrice = StopPrice;
            }

            public virtual bool UpdatePrice(decimal LastPrice)
            {
                if (!DirectionBuy)
                {
                    if (LastPrice < StopPrice)
                    {
                        return true;
                    }
                }
                else
                {
                    if (LastPrice > StopPrice)
                    {
                        return true;
                    }
                }
                return false;
            }

            public virtual string ShowInfo()
            {
                return String.Format("Тип: {0}, сума: {1}, StopPrice: {2}",
                   DirectionBuy ? "покупка" : "продажа", Amount, StopPrice);
            }

            public virtual string GetStateData()
            {
                return String.Format("{0};{1};{2};", DirectionBuy, Amount, StopPrice);
            }

            public static StopOrder LoadStateData(string Data)
            {
                string[] sData = Data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                return new StopOrder(Convert.ToBoolean(sData[0]),
                                     Convert.ToDecimal(sData[1]),
                                     Convert.ToDecimal(sData[2]));
            }
        }

        public class TrailingStopOrder : StopOrder
        {
            public decimal BestPrice { get; protected set; } //уровень цены за которым следовать

            public TrailingStopOrder(bool DirectionBuy, decimal Amount, decimal StopPrice, decimal MarketPrice)
                : base(DirectionBuy, Amount, StopPrice)
            {
                this.BestPrice = MarketPrice;
            }

            public override bool UpdatePrice(decimal LastPrice)
            {
                if (base.UpdatePrice(LastPrice))
                {
                    return true;
                }

                if (!DirectionBuy)
                {
                    if (LastPrice > BestPrice)
                    {
                        StopPrice += LastPrice - BestPrice;
                        BestPrice = LastPrice;
                    }
                }
                else
                {
                    if (LastPrice < BestPrice)
                    {
                        StopPrice -= BestPrice - LastPrice;
                        BestPrice = LastPrice;
                    }
                }
                return false;
            }

            public override string GetStateData()
            {
                return base.GetStateData() + String.Format("{0};", BestPrice);
            }

            public new static TrailingStopOrder LoadStateData(string Data)
            {
                string[] sData = Data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                return new TrailingStopOrder(Convert.ToBoolean(sData[0]),
                    Convert.ToDecimal(sData[1]),
                    Convert.ToDecimal(sData[2]),
                    Convert.ToDecimal(sData[3]));
            }
        }

        public class TrailingStopWaitOrder : TrailingStopOrder
        {
            public decimal WaitPrice { get; private set; }
            public bool Activate { get; private set; }

            public TrailingStopWaitOrder(bool DirectionBuy, decimal Amount, decimal StopPrice, decimal MarketPrice, decimal WaitPrice, bool Activate = false)
                : base(DirectionBuy, Amount, StopPrice, MarketPrice)
            {
                this.WaitPrice = WaitPrice;
                this.Activate = Activate;
            }

            public override bool UpdatePrice(decimal LastPrice)
            {
                if (Activate && base.UpdatePrice(LastPrice))
                {
                    return true;
                }

                if (Activate == false)
                {
                    if (!DirectionBuy)
                    {
                        if (LastPrice > WaitPrice)
                            Activate = true;
                    }
                    else
                    {
                        if (LastPrice < WaitPrice)
                            Activate = true;
                    }
                }
                return false;
            }

            public override string ShowInfo()
            {
                return base.ShowInfo() + String.Format(", WaitPrice: {0}, Activate: {1}", WaitPrice, Activate);
            }

            public override string GetStateData()
            {
                return base.GetStateData() + String.Format("{0};{1}", WaitPrice, Activate);
            }

            public new static TrailingStopWaitOrder LoadStateData(string Data)
            {
                string[] sData = Data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                return new TrailingStopWaitOrder(Convert.ToBoolean(sData[0]),
                    Convert.ToDecimal(sData[1]),
                    Convert.ToDecimal(sData[2]),
                    Convert.ToDecimal(sData[3]),
                    Convert.ToDecimal(sData[4]),
                    Convert.ToBoolean(sData[5]));
            }
        }
    }
}
