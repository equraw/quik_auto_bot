﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuikSharp.DataStructures;
using OfficeOpenXml;
using System.Xml;
using OfficeOpenXml.Style;
using System.Globalization;
using System.IO;
using System.Drawing;

namespace QuikAutoBot
{
    public static class ExcelStuff
    {
        public struct EntranceStat
        {
            public string StrategyName; //Стратегия 

            public DateTime OpenTime; // Дата и время открытия
            public Operation OpenOperation; //Операция
            public decimal OpenPrice; // Цена открытия
            public int OpenQuantityOnePrice; // Кол-во по одной цене
            public decimal OpenAveragePrice; // Средняя цена открытия

            public string Amount; // Кол-во 
        }

        public struct OutcomeStat
        {
            public DateTime CloseTime; // Дата и время закрытия
            public Operation CloseOperation; // Операция 
            public decimal ClosePrice; // Цена закрытия
            public int CloseQuantityOnePrice; // Кол-во по одной цене
            public decimal CloseAveragePrice; // Средняя цена закрытия
        }

        private struct CellData
        {
            public int Column;
            public Color FillColor;
            public string Data;
        }

        private static Dictionary<int, string> ColumnsInfo = new Dictionary<int, string> {
            { 1, "Стратегия" },
            { 2, "Инструмент"},
            { 3, "Дата и время открытия" },
            { 4, "Операция"},
            { 5, "Цена открытия"},
            { 6, "Кол-во по одной цене"},
            { 7, "Средняя цена открытия"},
            { 8, "Дата и время закрытия"},
            { 9, "Операция"},
            { 10, "Цена закрытия"},
            { 11, "Кол-во по одной цене"},
            { 12, "Средняя цена закрытия"},
            { 13, "Результат"},
            { 14, "Кол-во"},
            { 15, "Нарастающий итог"} };

        public static void SaveStat(string ToolName, string StatType, EntranceStat EntranceData)
        {
            List<CellData> data = new List<CellData>();
            WriteToExcel(ToolName, StatType, data, true);
        }

        public static void SaveStat(string ToolName, string StatType, OutcomeStat OutcomeData)
        {
            List<CellData> data = new List<CellData>();
            WriteToExcel(ToolName, StatType, data, false);
        }

        public static void Test()
        {
            var newFile = new FileInfo("TestFile.xlsx");
            using (var package = new ExcelPackage(newFile))
            {
                int ContainNumb = -1;
                for (int i = 1; i <= package.Workbook.Worksheets.Count; i++)
                {
                    if (package.Workbook.Worksheets[i].Name == "WorkSheet")
                    {
                        ContainNumb = i;
                        break;
                    }
                }
                ExcelWorksheet worksheet = (ContainNumb == -1) ? package.Workbook.Worksheets.Add("WorkSheet") :
                    package.Workbook.Worksheets[ContainNumb];

                int CurWriteRow = 1;
                if (worksheet.Dimension != null)
                {
                    CurWriteRow = worksheet.Dimension.End.Row + 1;
                }
                int CurColumn = 1;
                int nvalues = 5;

                for (int i = 0; i <= nvalues - 1; i++)
                {
                    worksheet.Cells[CurWriteRow + i, CurColumn].Value = "data " + (i + 1).ToString();
                    worksheet.Cells[CurWriteRow + 1, CurColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[CurWriteRow + 1, CurColumn].Style.Fill.BackgroundColor.SetColor(Color.Beige);
                }

                worksheet.Cells[CurWriteRow, CurColumn + 1].Value = "value 1";
                worksheet.Cells[CurWriteRow, CurColumn + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[CurWriteRow, CurColumn + 1].Style.Fill.BackgroundColor.SetColor(Color.PaleGreen);

                worksheet.Cells[CurWriteRow, CurColumn + 1, CurWriteRow + nvalues - 1, CurColumn + 1].Merge = true;
                worksheet.Cells[CurWriteRow, CurColumn + 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[CurWriteRow, CurColumn + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                package.Save();
            }
        }

        private static void WriteToExcel(string FileName, string WorksheetName, List<CellData> data, bool InNewRow)
        {
            var newFile = new FileInfo(FileName);
            using (var package = new ExcelPackage(newFile))
            {
                int ContainNumb = -1;
                for (int i = 1; i <= package.Workbook.Worksheets.Count; i++)
                {
                    if (package.Workbook.Worksheets[i].Name == WorksheetName)
                    {
                        ContainNumb = i;
                        break;
                    }
                }
                ExcelWorksheet worksheet = (ContainNumb == -1) ? package.Workbook.Worksheets.Add(WorksheetName) :
                    package.Workbook.Worksheets[ContainNumb];

                int CurWriteRow = 1;
                if (worksheet.Dimension != null)
                {
                    CurWriteRow = (InNewRow) ? worksheet.Dimension.End.Row + 1 : worksheet.Dimension.End.Row;
                }
                else //новый лист, добавление оглавления
                {
                    foreach (var cdata in data)
                    {
                        worksheet.Cells[CurWriteRow, cdata.Column].Value = ColumnsInfo[cdata.Column];
                        worksheet.Cells[CurWriteRow, cdata.Column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[CurWriteRow, cdata.Column].Style.Fill.BackgroundColor.SetColor(Color.DarkOrange);
                    }
                    CurWriteRow += 1;
                }
                foreach (var cdata in data)
                {
                    worksheet.Cells[CurWriteRow, cdata.Column].Value = cdata.Data;
                    if (cdata.FillColor != null)
                    {
                        worksheet.Cells[CurWriteRow, cdata.Column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[CurWriteRow, cdata.Column].Style.Fill.BackgroundColor.SetColor(cdata.FillColor);
                    }
                }
                package.Save();
            }
        }
    }
}
