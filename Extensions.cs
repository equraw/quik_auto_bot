﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace QuikAutoBot
{
    /// <summary>
    /// Необходимые параметры стратегии
    /// </summary>
    public struct StrategyParam
    {
        public string StrategyName; // название стратегии
        public string SecCode; //код инструмента
        public string ShortTag; //метка индикатора short
        public string LongTag; //метка индикатора long
        public double DiffPerc; //% необходимого разрыва для сигнала
        public int BalancePerc; //% от баланса
        public DateTime SetTime; //время остановки стратегии
        public double TStopDiff; //% на который ставить Trailing Stop Order
        public double SimpleStopValue; //расстояие для выставления простого стоп-ордера
        public bool SetTrailingStop; //ставить плавающий стоп
        public bool SetSimpleStop; //ставить простой стоп

        public static void SaveParam(StrategyParam Param, string SectorName)
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
                {
                    { "StrategyName", Param.StrategyName},
                    { "SecCode", Param.SecCode},
                    { "MA_short_ID", Param.ShortTag},
                    { "MA_long_ID", Param.LongTag},
                    { "Minimum_Difference",Param.DiffPerc.ToString()},
                    { "BalancePercent", Param.BalancePerc.ToString()},
                    { "SetTime", Param.SetTime.ToString()},
                    { "TStopDiff", Param.TStopDiff.ToString()},
                    { "SimpleStopValue", Param.SimpleStopValue.ToString()},
                    { "SetTrailingStop", Param.SetTrailingStop.ToString()},
                    { "SetSimpleStop", Param.SetSimpleStop.ToString()},
                };
            IniTool.ChangeValue(Form1.IniFname, SectorName, data);
        }

        public static StrategyParam LoadParam(Dictionary<string, string> dict)
        {
            StrategyParam Param = new StrategyParam();
            if (dict != null)
            {
                Param.StrategyName = dict["StrategyName"];
                Param.SecCode = dict["SecCode"];
                Param.ShortTag = dict["MA_short_ID"];
                Param.LongTag = dict["MA_long_ID"];
                Param.DiffPerc = Convert.ToDouble(dict["Minimum_Difference"].Replace(',', '.'), CultureInfo.InvariantCulture);
                Param.BalancePerc = Convert.ToInt32(dict["BalancePercent"]);
                Param.SetTime = Convert.ToDateTime(dict["SetTime"]);
                Param.TStopDiff = Convert.ToDouble(dict["TStopDiff"].Replace(',', '.'), CultureInfo.InvariantCulture);
                Param.SimpleStopValue = Convert.ToDouble(dict["SimpleStopValue"].Replace(',', '.'), CultureInfo.InvariantCulture);
                Param.SetTrailingStop = Convert.ToBoolean(dict["SetTrailingStop"]);
                Param.SetSimpleStop = Convert.ToBoolean(dict["SetSimpleStop"]);
            }
            return Param;
        }
    }

    /// <summary>
    /// Вся информация про состояние стратегии (без параметров)
    /// </summary>
    public struct StrategyState
    {
        public decimal PrevMAShort;
        public decimal PrevMALong;
        public decimal LastMAShort; //не сохраняется
        public decimal LastMALong; //не сохраняется
        public int PrevCross;//Зафиксированное пересечение: 0 - нету, 1 - на лонг, -1 - на шорт
        public int QtyInPos; //количество в позиции уже
        public bool IsStartegyRun; //запущена ли стратегия

        internal int posDirection; //Текущая позиция: 0 - нету, 1 - лонг, -1 - шорт
        public int FuturePos; //0 - нету, 1 - на лонг, -1 - на шорт
        public bool FutureStop; //остановить на следующем сигнале

        internal bool isTimerRun; //запущен ли таймер на остановку стратегии
        public DateTime InterrupTime; //время последнего обновления

        public ExTool.TrailingStopOrder TStop1; //плавающий стоп
        public ExTool.StopOrder SimpleStop1; //простой стоп

        /// <summary>
        /// Сохраняет информацию про позицию с текущим временем
        /// </summary>
        /// <param name="Fname"></param>
        public static void SaveState(StrategyState State, string SectorName)
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
                {
                { "PrevMAShort", State.PrevMAShort.ToString() },
                { "PrevMALong", State.PrevMALong.ToString()},
                { "PrevCross", State.PrevCross.ToString()},
                { "QtyInPos", State.QtyInPos.ToString() },
                { "IsStartegyRun", State.IsStartegyRun.ToString()}, //обработка в Form1

                { "PosDirection", State.posDirection.ToString() },
                { "FuturePos", State.FuturePos.ToString()},
                { "FutureStop", State.FutureStop.ToString() },

                { "IsTimerRun", State.isTimerRun.ToString()},
                { "InterrupTime", DateTime.Now.ToString()},

                { "StayTStop", State.TStop1 == null ? "" : State.TStop1.GetStateData() },
                { "StaySimpleStop", State.SimpleStop1 == null ? "" : State.SimpleStop1.GetStateData()},
                };
            IniTool.ChangeValue(Form1.IniFname, SectorName, data);
        }

        /// <summary>
        /// Загружает информацию про позицию если выполняется ограничение времени
        /// </summary>
        /// <param name="state"></param>
        public static StrategyState LoadState(Dictionary<string, string> dict)
        {
            StrategyState State = new StrategyState();
            if (dict != null)
            {
                State.PrevMAShort = Convert.ToDecimal(dict["PrevMAShort"].Replace(',', '.'), CultureInfo.InvariantCulture);
                State.PrevMALong = Convert.ToDecimal(dict["PrevMALong"].Replace(',', '.'), CultureInfo.InvariantCulture);
                State.PrevCross = Convert.ToInt32(dict["PrevCross"]);
                State.QtyInPos = Convert.ToInt32(dict["QtyInPos"]);

                State.posDirection = Convert.ToInt32(dict["PosDirection"]);
                State.FuturePos = Convert.ToInt32(dict["FuturePos"]);
                State.FutureStop = Convert.ToBoolean(dict["FutureStop"]);

                State.isTimerRun = Convert.ToBoolean(dict["IsTimerRun"]);
                State.InterrupTime = DateTime.Parse(dict["InterrupTime"]);

                State.TStop1 = String.IsNullOrEmpty(dict["StayTStop"]) ? null :
                    ExTool.TrailingStopOrder.LoadStateData(dict["StayTStop"]);

                State.SimpleStop1 = String.IsNullOrEmpty(dict["StaySimpleStop"]) ? null :
                    ExTool.StopOrder.LoadStateData(dict["StaySimpleStop"]);
            }
            return State;
        }
    }
}
