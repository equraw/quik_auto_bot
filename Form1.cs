﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuikSharp;
using QuikSharp.DataStructures;
using QuikSharp.DataStructures.Transaction;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using IniParser;
using IniParser.Model;
using System.Collections.Concurrent;
using System.Timers;
using System.Globalization;

namespace QuikAutoBot
{
    public delegate void InvokePrint(string Text, bool PrintTime = false);

    public partial class Form1 : Form
    {
        public static string IniFname = "Settings.ini";
        private object LockObj = new object();

        private InvokePrint Print;
        private List<Strategy> StrategiesList = new List<Strategy>(); //все стратегии
        private Strategy ActiveStrategy = null; //активная стратегия
        private List<string> NeedRun = new List<string>();
        private int _activeIndex;
        private int ActiveIndex //индекс активной стратегии
        {
            get { return _activeIndex; }
            set
            {
                lock (LockObj)
                {
                    if (ActiveStrategy != null)
                    {
                        ActiveStrategy.OnChangePosition -= OnStrategyChangePosition;
                        ActiveStrategy.OnChangeState -= OnStartegyChangeState;
                        ActiveStrategy.OnChangeIndictors -= StartChangeIndicators;
                        ActiveStrategy.OnConnectTool -= OnStrategyConnectTool;
                        if (ActiveStrategy.Tool != null)
                        {
                            try
                            {
                                ActiveStrategy.Tool.OnNewPrices -= OnChangePrices;
                                ActiveStrategy.Tool.UnsubscribeQuote();
                            }
                            catch (Exception ex)
                            {
                                System.Media.SystemSounds.Beep.Play();
                                Print("Ошибка при отписывании на стакан: " + ex.Message, true);
                            }
                        }
                    }
                    ActiveStrategy = StrategiesList[value];
                    SetActiveStrategy(ActiveStrategy);
                    _activeIndex = value;

                    ActiveStrategy.OnChangePosition += OnStrategyChangePosition;
                    ActiveStrategy.OnChangeState += OnStartegyChangeState;
                    ActiveStrategy.OnChangeIndictors += StartChangeIndicators;

                    if (ActiveStrategy.Tool != null)
                    {
                        ActiveStrategy.Tool.OnNewPrices += OnChangePrices;
                        if (ActiveStrategy.Tool.SubscribeQuote() != true)
                        {
                            System.Media.SystemSounds.Beep.Play();
                            Print("Не удалось подписаться на обновления стакана!", true);
                        }
                    }
                    else
                    {
                        ActiveStrategy.OnConnectTool += OnStrategyConnectTool;
                    }

                    int index = NeedRun.FindIndex((x) => x == ActiveStrategy.Param.StrategyName);
                    if (index >= 0)
                    {
                        NeedRun.RemoveAt(index);
                        try
                        {
                            ThreadPool.QueueUserWorkItem((x) => ActiveStrategy.Start(FSetting.FreqUpdate));
                        }
                        catch (Exception ex)
                        {
                            System.Media.SystemSounds.Beep.Play();
                            Print("Ошибка при запуске стратегии: " + ex.Message, true);
                        }
                    }
                }
            }
        }
        private System.Threading.Timer timerRestart;

        public struct FormSettings
        {
            public int FreqRestart; //Время перезагрузки (часов):
            public int FreqRestartM; //Время перезагрузки (минут):
            public int FreqUpdate; //Частота обновления индикаторов (сек):
        }

        FormSettings FSetting;
        private ConcurrentBag<string> FailUnsubscr = new ConcurrentBag<string>(); //от чего стакана не удалось отписаться

        public Form1()
        {
            InitializeComponent();
            Print = new InvokePrint(MPrint);
            QuikInstance.Print = this.Print;
            IniTool.Print = this.Print;

            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/dd/yyyy HH:mm:ss";

            ReadFormSettings();
            SetRestartTimer(FSetting.FreqRestart, FSetting.FreqRestartM);

            ReadStrategies();
        }

        private void ReadFormSettings()
        {
            if (File.Exists(IniFname))
            {
                try
                {
                    FileIniDataParser fileIniData = new FileIniDataParser();
                    IniData parsedData = fileIniData.ReadFile(IniFname);
                    Dictionary<string, string> data = IniTool.ReadSection(IniFname, "FormSettings");
                    FSetting.FreqRestart = Convert.ToInt32(data["FreqRestart"]);
                    FSetting.FreqRestartM = Convert.ToInt32(data["FreqRestartM"]);
                    FSetting.FreqUpdate = Convert.ToInt32(data["FreqUpdate"]);
                }
                catch (Exception ex)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Ошибка чтения FormSettings: " + ex.Message);
                }
            }
            else
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData newParsedData = new IniData();
                newParsedData.Sections.AddSection("FormSettings");
                SectionData section = newParsedData.Sections.GetSectionData("FormSettings");
                section.Keys.AddKey("FreqRestart", "4");
                section.Keys.AddKey("FreqUpdate", "5");
                fileIniData.WriteFile(IniFname, newParsedData);
            }
        }

        private void SetRestartTimer(int hour, int minute)
        {
            try
            {
                if (timerRestart != null)
                {
                    timerRestart.Dispose();
                }
                timerRestart = new System.Threading.Timer(RestartProgram);
                DateTime now = DateTime.Now;
                DateTime next = new DateTime(now.Year, now.Month, now.Day, hour, minute, 0);
                if (now.AddSeconds(5) >= next)
                {
                    next = next.AddDays(1);
                }

                timerRestart.Change(next - now, new TimeSpan(-1));
                Print("Таймер на перезагрузку запущено на " + next.ToString());
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка установки таймера перезагрузки программы: " + ex.Message, true);
            }
        }

        public void UpdateSettings(FormSettings settings)
        {
            try
            {
                if (FSetting.FreqRestart != settings.FreqRestart ||
                    FSetting.FreqRestartM != settings.FreqRestartM)
                {
                    SetRestartTimer(settings.FreqRestart, settings.FreqRestartM);
                }
                if (FSetting.FreqUpdate != settings.FreqUpdate)
                {
                    for (int i = 0; i < StrategiesList.Count; i++)
                    {
                        StrategiesList[i].timerUpdate.Change(TimeSpan.FromSeconds(settings.FreqUpdate),
                            TimeSpan.FromSeconds(settings.FreqUpdate));
                    }
                }
                FSetting = settings;
                IniTool.ChangeValue(IniFname, "FormSettings", new Dictionary<string, string> {
                {"FreqRestart", FSetting.FreqRestart.ToString() },
                {"FreqRestartM", FSetting.FreqRestartM.ToString() },
                {"FreqUpdate", FSetting.FreqUpdate.ToString() }
                });
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка применения настроек: " + ex.Message);
            }
        }

        private void OnStrategyConnectTool()
        {
            //ActiveStrategy.Tool.TestCall();
            ActiveStrategy.Tool.OnNewPrices += OnChangePrices;
            if (ActiveStrategy.Tool.SubscribeQuote() != true)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Не удалось подписаться на обновления стакана!", true);
            }
        }

        /// <summary>
        /// Прочитать стратегии из ini файла
        /// </summary>
        private void ReadStrategies()
        {
            if (File.Exists(IniFname))
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData parsedData = fileIniData.ReadFile(IniFname);
                var StratSect = parsedData.Sections.Where((x) => { return x.SectionName.Contains("Strategy_"); });
                foreach (var sdata in StratSect)
                {
                    try
                    {
                        Dictionary<string, string> ResDict = new Dictionary<string, string>();
                        for (int i = 0; i < sdata.Keys.Count; i++)
                        {
                            ResDict.Add(sdata.Keys.ElementAt(i).KeyName, sdata.Keys.ElementAt(i).Value);
                        }
                        StrategyParam sParam = StrategyParam.LoadParam(ResDict);
                        StrategyState sState = StrategyState.LoadState(ResDict);
                        Strategy StratMA = new Strategy(this.Print, sParam, sState);
                        if (Convert.ToBoolean(ResDict["IsStartegyRun"]))
                        {
                            NeedRun.Add(StratMA.Param.StrategyName);
                        }

                        StrategiesList.Add(StratMA);
                        listBox1.Items.Add(StratMA.Param.StrategyName);
                        listBox1.SelectedIndex = listBox1.Items.Count - 1;
                        Thread.Sleep(400);
                    }
                    catch (Exception ex)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Ошибка при возобновлении стратегии из ini файла: " + ex.Message);
                    }
                }
            }
            else
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData newParsedData = new IniData();
                fileIniData.WriteFile(IniFname, newParsedData);
            }
        }

        /// <summary>
        /// Смена текущей выбранной стратегии для редактирования
        /// </summary>
        /// <param name="strategy"></param>
        private void SetActiveStrategy(Strategy strategy)
        {
            if (strategy == null)
            {
                buttonRun.Enabled = false;
                buttonPause.Enabled = false;
                buttonStop.Enabled = false;
                buttonDelete.Enabled = false;
                return;
            }
            //управление
            buttonRun.Enabled = !ActiveStrategy.State.IsStartegyRun;
            buttonPause.Enabled = !ActiveStrategy.State.IsStartegyRun;
            buttonStop.Enabled = ActiveStrategy.State.IsStartegyRun;
            buttonDelete.Enabled = true;

            //информация
            textBoxCurPos.Text = Strategy.PositionToString(ActiveStrategy.PosDirection);
            textBoxLastMAShort.Text = ActiveStrategy.State.LastMAShort.ToString();
            textBoxLastMALong.Text = ActiveStrategy.State.LastMALong.ToString();
            textBoxLastPrice.Text = "";
            textBoxBestOffer.Text = "";
            textBoxBestBid.Text = "";

            //параметры
            textBoxStrategyName.Text = ActiveStrategy.Param.StrategyName;
            textBoxSecCode.Text = ActiveStrategy.Param.SecCode;
            textBoxShortTag.Text = ActiveStrategy.Param.ShortTag;
            textBoxLongTag.Text = ActiveStrategy.Param.LongTag;
            textBoxDiffPerc.Text = ActiveStrategy.Param.DiffPerc.ToString();
            textBoxBalancePerc.Text = ActiveStrategy.Param.BalancePerc.ToString();
            textBox1.Text = ActiveStrategy.Param.TStopDiff.ToString();
            textBox2.Text = ActiveStrategy.Param.SimpleStopValue.ToString();
            checkBox2.Checked = ActiveStrategy.Param.SetTrailingStop;
            checkBox3.Checked = ActiveStrategy.Param.SetSimpleStop;
            try
            {
                dateTimePicker1.Value = ActiveStrategy.Param.SetTime;
            }
            catch
            {
                dateTimePicker1.Value = dateTimePicker1.MinDate;
            }
        }

        /// <summary>
        /// Выаод последних значений индикаторов
        /// </summary>
        /// <param name="Short"></param>
        /// <param name="Long"></param>
        private void StartChangeIndicators(decimal Short, decimal Long)
        {
            textBoxLastMAShort.Invoke(new Action(() => { textBoxLastMAShort.Text = Short.ToString(); }));
            textBoxLastMALong.Invoke(new Action(() => { textBoxLastMALong.Text = Long.ToString(); }));
        }

        /// <summary>
        /// Возвращает результат таска или генерирует исключение по тайм-ауту
        /// </summary>
        /// <param name="UnknownTask"></param>
        /// <returns></returns>
        public static dynamic ExecuteQuery(dynamic UnknownTask, int MsTime = 20000)
        {
            if (UnknownTask.Wait(MsTime) == true)
            {
                return UnknownTask.Result;
            }
            else
            {
                throw new Exception("Время ожидания результата истекло!");
            }
        }

        #region StratMA_Related

        /// <summary>
        /// Запуск стратегии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRun_Click(object sender, EventArgs e)
        {
            buttonRun.Enabled = false;
            if (ActiveStrategy == null)
            {
                buttonRun.Enabled = true;
                return;
            }
            StrategyParam? sParam = ReadStrategyParam(true);
            if (sParam == null)
            {
                buttonRun.Enabled = true;
                return;
            }
            if (((StrategyParam)sParam).StrategyName != ActiveStrategy.Param.StrategyName) //переименование
            {
                listBox1.Items[listBox1.SelectedIndex] = ((StrategyParam)sParam).StrategyName;
                string sectname = IniTool.ConvertName("Strategy_" + ActiveStrategy.Param.StrategyName);
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData parsedData = fileIniData.ReadFile(IniFname);
                if (parsedData.Sections.ContainsSection(sectname))
                {
                    parsedData.Sections.RemoveSection(sectname);
                    fileIniData.WriteFile(IniFname, parsedData);
                }
            }
            ThreadPool.QueueUserWorkItem((x) => ActiveStrategy.Start(FSetting.FreqUpdate, sParam));
        }

        private StrategyParam? ReadStrategyParam(bool ForRun)
        {
            try
            {
                if (textBoxStrategyName.Text == "" || textBoxSecCode.Text == "" || textBoxShortTag.Text == "" ||
                            textBoxLongTag.Text == "" || textBoxDiffPerc.Text == "" || textBoxBalancePerc.Text == "")
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Заполнены не все поля!");
                    return null;
                }
                string sName = textBoxStrategyName.Text;

                for (int i = 0; i < StrategiesList.Count; i++)
                {
                    if (StrategiesList[i].Param.StrategyName == sName)
                    {
                        if (ForRun)
                        {
                            if (ActiveIndex == i)
                                continue; //стратегия уже существует, допустимо совпадение с собой
                        }
                        System.Media.SystemSounds.Beep.Play();
                        Print("Такое название уже существует!");
                        return null;
                    }
                }
                StrategyParam sParam = new StrategyParam
                {
                    StrategyName = sName,
                    SecCode = textBoxSecCode.Text,
                    ShortTag = Convert.ToString(textBoxShortTag.Text),
                    LongTag = Convert.ToString(textBoxLongTag.Text),
                    DiffPerc = Convert.ToDouble(textBoxDiffPerc.Text.Replace(",", "."), CultureInfo.InvariantCulture),
                    BalancePerc = Convert.ToInt32(textBoxBalancePerc.Text),
                    TStopDiff = Convert.ToDouble(textBox1.Text.Replace(",", "."), CultureInfo.InvariantCulture),
                    SimpleStopValue = Convert.ToDouble(textBox2.Text.Replace(",", "."), CultureInfo.InvariantCulture),
                    SetTrailingStop = checkBox2.Checked,
                    SetSimpleStop = checkBox3.Checked
                };
                return sParam;
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка при чтении параметров стратегии: " + ex.Message, true);
                return null;
            }
        }

        /// <summary>
        /// Остановка стратегии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy == null)
            {
                return;
            }
            try
            {
                if (ActiveStrategy.State.IsStartegyRun)
                {
                    buttonStop.Enabled = false;
                    ThreadPool.QueueUserWorkItem((x) => ActiveStrategy.Stop());
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                buttonStop.Enabled = true;
                Print("Ошибка при остановке стратегии: " + ex.Message, true);
            }
        }

        /// <summary>
        /// Изменение стакана
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="offer"></param>
        private void OnChangePrices(decimal bid, decimal offer)
        {
            this.Invoke(new Action(() =>
            {
                textBoxLastPrice.Text = ActiveStrategy.Tool.LastPrice.ToString();
                textBoxBestOffer.Text = offer.ToString();
                textBoxBestBid.Text = bid.ToString();
            }));
        }

        /// <summary>
        /// Происходит при смене состояния бота, его отображение
        /// </summary>
        /// <param name="Position"></param>
        private void OnStrategyChangePosition(int Position)
        {
            string strpos = Strategy.PositionToString(Position);
            if (textBoxCurPos.InvokeRequired)
            {
                textBoxCurPos.Invoke(new Action(() => { textBoxCurPos.Text = strpos; }));
            }
            else
            {
                textBoxCurPos.Text = strpos;
            }
        }

        /// <summary>
        /// Получает результат запуска или остановки стратегии.
        /// </summary>
        /// <param name="state"></param>
        private void OnStartegyChangeState(bool FromRun, bool Result)
        {
            if (FromRun == true)
            {
                if (Result == true)
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action(() =>
                        {
                            buttonRun.Enabled = false;
                            buttonPause.Enabled = false;
                            buttonStop.Enabled = true;
                        }));
                    }
                    Print("Стратегия запущена успешно.", true);
                }
                else
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action(() =>
                        {
                            buttonRun.Enabled = true;
                            buttonPause.Enabled = true;
                            buttonStop.Enabled = false;
                        }));
                    }
                    Print("Стратегию не запущено.");
                }
            }
            else
            {
                if (Result == true)
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action(() =>
                        {
                            buttonRun.Enabled = true;
                            buttonPause.Enabled = true;
                            buttonStop.Enabled = false;
                        }));
                    }
                    Print("Стратегия остановлена успешно.", true);
                }
                else
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action(() =>
                        {
                            buttonRun.Enabled = false;
                            buttonPause.Enabled = false;
                            buttonStop.Enabled = true;
                        }));
                    }
                    Print("Стратегию не остановлено.");
                }
            }
        }

        /// <summary>
        /// Войти в позицию вручную
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy == null)
            {
                return;
            }
            if (ActiveStrategy.State.IsStartegyRun == true)
            {
                button2.Enabled = false;
                int state = 0;
                if (radioButton1.Checked)
                    state = -1;
                if (radioButton2.Checked)
                    state = 1;
                if (radioButton6.Checked)
                    state = 0;
                if (checkBox1.Checked)
                {
                    ActiveStrategy.ChangeState(state, true, true);
                }
                else
                {
                    ActiveStrategy.ChangeState(state, true, false);
                }
                button2.Enabled = true;
            }
        }

        /// <summary>
        /// Изменение позиции вручную без сделки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy == null)
            {
                return;
            }
            if (ActiveStrategy.State.IsStartegyRun == true)
            {
                button3.Enabled = false;
                int state = 0;
                if (radioButton1.Checked)
                    state = -1;
                if (radioButton2.Checked)
                    state = 1;
                if (radioButton6.Checked)
                    state = 0;
                ActiveStrategy.ChangeState(state, false, false);
                button3.Enabled = true;
            }
        }

        #endregion

        /// <summary>
        /// Выаод информации
        /// </summary>
        /// <param name="text"></param>
        private void MPrint(string text, bool ptime)
        {
            string ptext = String.Format("{0} - {1}{2}", ptime == false ? "" : String.Format("{0}", DateTime.Now.ToString()),
                text, Environment.NewLine);
            if (textBoxLogsWindow.InvokeRequired)
            {
                textBoxLogsWindow.Invoke(new Action<string>((str) =>
                {
                    textBoxLogsWindow.AppendText(str);
                }), ptext);
            }
            else
            {   
                textBoxLogsWindow.AppendText(ptext);
            }
        }

        /// <summary>
        /// Отписка от лишних стаканов
        /// </summary>
        /// <param name="class_code"></param>
        /// <param name="sec_code"></param>
        private void Unsubscribe(string class_code, string sec_code)
        {
            string unicsec = sec_code + class_code;
            if (FailUnsubscr.Contains(unicsec) == true)
            {
                return;
            }
            Print(String.Format("Отписываемся от предыдущего стакана sec_code: {0}, class_code: {1}",
                sec_code, class_code));
            if (ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(class_code, sec_code)) == false)
            {
                Print("Подписки не было! " + sec_code + " " + class_code);
                FailUnsubscr.Add(unicsec);
                return;
            }
            else
            {
                bool unres = ExecuteQuery(QuikInstance.quik.OrderBook.Unsubscribe(class_code, sec_code));
                if (unres == false)
                {
                    Print("Успешно.", false);
                }
                else
                {
                    FailUnsubscr.Add(unicsec);
                    Print(unicsec + " неуспешно " + FailUnsubscr.Count);
                }
            }
        }

        /// <summary>
        /// Закрытие
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            for (int i = 0; i < StrategiesList.Count; i++)
            {
                StrategiesList[i].SaveState();
                DisposeResources(StrategiesList[i].Tool);
            }
            if (QuikInstance.quik != null)
                if (QuikInstance.quik.OrderBook.QuikService.IsStarted)
                {
                    QuikInstance.quik.OrderBook.QuikService.Stop();
                }
        }

        /// <summary>
        /// Исполняется при закрытии приложения
        /// </summary>
        private void DisposeResources(Tool tool)
        {
            try
            {
                if (tool != null)
                {
                    if (ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(tool.ClassCode, tool.SecurityCode)))
                    {
                        if (ExecuteQuery(QuikInstance.quik.OrderBook.Unsubscribe(tool.ClassCode, tool.SecurityCode)) != true)
                        {
                            Print("Не удалось отписаться от получения стакана.");
                        }
                    }
                    //Print("Освобождение ресурсов завершено.", true);
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Произошла ошибка: " + ex.Message, true);
            }
        }

        /// <summary>
        /// Смена % от баланса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy == null)
            {
                return;
            }
            int BalPerc = Convert.ToInt32(textBoxBalancePerc.Text);
            Interlocked.Exchange(ref ActiveStrategy.Param.BalancePerc, BalPerc);
            Print("Новый % от баланса сохранен.", true);
        }

        /// <summary>
        /// Очистить лог
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            textBoxLogsWindow.Clear();
        }

        private void RestartProgram(object state)
        {
            ProcessStartInfo processInfo = new ProcessStartInfo
            {
                FileName = "Refresh.exe",
                ErrorDialog = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                Arguments = "10"
            };
            string path = AppDomain.CurrentDomain.BaseDirectory;
            processInfo.WorkingDirectory = Path.GetDirectoryName(path);
            Process proc = Process.Start(processInfo);
            this.Invoke(new Action(() => { this.Close(); }));
        }

        /// <summary>
        /// Смена даты остановки торгов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy != null)
            {
                ActiveStrategy.SetTimerStop(dateTimePicker1.Value);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
                ActiveIndex = listBox1.SelectedIndex;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy == null)
            {
                return;
            }
            if (ActiveStrategy.State.IsStartegyRun)
            {
                buttonStop_Click(null, null);
                if (ActiveStrategy.State.IsStartegyRun)
                {
                    DialogResult result = MessageBox.Show("Не удалось остановить стратегию. Все равно удалить?", "Удаление стратегии",
                        MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
            }

            string sectname = IniTool.ConvertName("Strategy_" + ActiveStrategy.Param.StrategyName);
            int index = listBox1.SelectedIndex;
            StrategiesList.RemoveAt(index);
            listBox1.Items.RemoveAt(index);
            FileIniDataParser fileIniData = new FileIniDataParser();
            IniData parsedData = fileIniData.ReadFile(IniFname);
            if (parsedData.Sections.ContainsSection(sectname))
            {
                parsedData.Sections.RemoveSection(sectname);
                fileIniData.WriteFile(IniFname, parsedData);
            }
            if (listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = (index - 1) >= 0 ? index - 1 : 0;
            }
            else
            {
                SetActiveStrategy(null);
            }
        }

        /// <summary>
        /// Продолжить выполнение стратегии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPause_Click(object sender, EventArgs e)
        {
            buttonPause.Enabled = false;
            if (ActiveStrategy == null)
            {
                buttonPause.Enabled = true;
                return;
            }
            ThreadPool.QueueUserWorkItem((x) => ActiveStrategy.Start(FSetting.FreqUpdate));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Settings fset = new Settings(FSetting);
            fset.ShowDialog(this);
        }

        //to excel
        private void button8_Click(object sender, EventArgs e)
        {
            //ExcelStuff.WriteToExcel();
        }

        //Добавить новую стратегию
        private void button8_Click_1(object sender, EventArgs e)
        {
            StrategyParam? sParam = ReadStrategyParam(false);
            if (sParam == null)
            {
                return;
            }
            Strategy NewStrategy = new Strategy(this.Print, (StrategyParam)sParam, new StrategyState());

            StrategiesList.Add(NewStrategy);
            listBox1.Items.Add(NewStrategy.Param.StrategyName);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            ActiveStrategy.SaveState();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ExcelStuff.Test();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy != null)
            {
                int CurBalancePerc = 0;
                if (ActiveStrategy.Tool != null)
                {
                    try
                    {
                        CurBalancePerc = ActiveStrategy.CurBalancePerc;
                    }
                    catch (Exception ex)
                    {
                        Print("Ошибка получения баланса: " + ex.Message);
                        System.Media.SystemSounds.Beep.Play();
                    }
                }
                else
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Tool == null", true);
                }
                ThreadPool.QueueUserWorkItem((x) => Application.Run(
                    new Information(ActiveStrategy.State, CurBalancePerc, ActiveStrategy.Tool,
                    ActiveStrategy.State.TStop1, ActiveStrategy.State.SimpleStop1)));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ActiveStrategy.Tool != null)
            {
                ActiveStrategy.Tool.TestCall();
            }
        }
    }
}
