﻿namespace QuikAutoBot
{
    partial class Information
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFirmID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxClientCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAccountID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxClassCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxGuaranteeProviding = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxStep = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxLot = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxShortName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // textBoxFirmID
            // 
            this.textBoxFirmID.Enabled = false;
            this.textBoxFirmID.Location = new System.Drawing.Point(85, 94);
            this.textBoxFirmID.Name = "textBoxFirmID";
            this.textBoxFirmID.Size = new System.Drawing.Size(140, 20);
            this.textBoxFirmID.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "FirmID";
            // 
            // textBoxClientCode
            // 
            this.textBoxClientCode.Enabled = false;
            this.textBoxClientCode.Location = new System.Drawing.Point(85, 41);
            this.textBoxClientCode.Name = "textBoxClientCode";
            this.textBoxClientCode.Size = new System.Drawing.Size(140, 20);
            this.textBoxClientCode.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "ClientCode";
            // 
            // textBoxAccountID
            // 
            this.textBoxAccountID.Enabled = false;
            this.textBoxAccountID.Location = new System.Drawing.Point(85, 68);
            this.textBoxAccountID.Name = "textBoxAccountID";
            this.textBoxAccountID.Size = new System.Drawing.Size(140, 20);
            this.textBoxAccountID.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "AccountID";
            // 
            // textBoxClassCode
            // 
            this.textBoxClassCode.Enabled = false;
            this.textBoxClassCode.Location = new System.Drawing.Point(85, 15);
            this.textBoxClassCode.Name = "textBoxClassCode";
            this.textBoxClassCode.Size = new System.Drawing.Size(140, 20);
            this.textBoxClassCode.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "ClassCode";
            // 
            // textBoxGuaranteeProviding
            // 
            this.textBoxGuaranteeProviding.Enabled = false;
            this.textBoxGuaranteeProviding.Location = new System.Drawing.Point(85, 205);
            this.textBoxGuaranteeProviding.Name = "textBoxGuaranteeProviding";
            this.textBoxGuaranteeProviding.Size = new System.Drawing.Size(140, 20);
            this.textBoxGuaranteeProviding.TabIndex = 56;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 208);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "ГО";
            // 
            // textBoxStep
            // 
            this.textBoxStep.Enabled = false;
            this.textBoxStep.Location = new System.Drawing.Point(85, 182);
            this.textBoxStep.Name = "textBoxStep";
            this.textBoxStep.Size = new System.Drawing.Size(140, 20);
            this.textBoxStep.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Шаг цены";
            // 
            // textBoxLot
            // 
            this.textBoxLot.Enabled = false;
            this.textBoxLot.Location = new System.Drawing.Point(85, 156);
            this.textBoxLot.Name = "textBoxLot";
            this.textBoxLot.Size = new System.Drawing.Size(140, 20);
            this.textBoxLot.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Lot";
            // 
            // textBoxShortName
            // 
            this.textBoxShortName.Enabled = false;
            this.textBoxShortName.Location = new System.Drawing.Point(85, 130);
            this.textBoxShortName.Name = "textBoxShortName";
            this.textBoxShortName.Size = new System.Drawing.Size(140, 20);
            this.textBoxShortName.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "ShortName";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(243, 15);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(322, 210);
            this.richTextBox1.TabIndex = 58;
            this.richTextBox1.Text = "";
            // 
            // Information
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 283);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBoxFirmID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxClientCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxAccountID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxClassCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxGuaranteeProviding);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxStep);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxLot);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxShortName);
            this.Controls.Add(this.label6);
            this.Name = "Information";
            this.Text = "Information";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFirmID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxClientCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAccountID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxClassCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxGuaranteeProviding;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxStep;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxLot;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxShortName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}