﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuikSharp.DataStructures;

namespace QuikAutoBot
{
    public partial class Information : Form
    {
        public Information()
        {
            InitializeComponent();
        }

        public Information(StrategyState sState)
        {
            InitializeComponent();
            ShowState(sState);
        }

        private void ShowState(StrategyState sState)
        {
            richTextBox1.Text += String.Format("Количество в позиции: {0}{1}", sState.QtyInPos, Environment.NewLine);

            richTextBox1.Text += String.Format("PrevMAShort: {0}{1}", sState.PrevMAShort, Environment.NewLine);
            richTextBox1.Text += String.Format("PrevMALong: {0}{1}", sState.PrevMALong, Environment.NewLine);
            richTextBox1.Text += String.Format("LastMAShort: {0}{1}", sState.LastMAShort, Environment.NewLine);
            richTextBox1.Text += String.Format("LastMALong: {0}{1}{2}", sState.LastMALong, Environment.NewLine, Environment.NewLine);

            richTextBox1.Text += String.Format("IsStartegyRun: {0}{1}", sState.IsStartegyRun, Environment.NewLine);
            richTextBox1.Text += String.Format("Зафиксированное пересечение: {0}{1}", Strategy.PositionToString(sState.PrevCross), Environment.NewLine);
            richTextBox1.Text += String.Format("Текущая позиция: {0}{1}", Strategy.PositionToString(sState.posDirection), Environment.NewLine);
            richTextBox1.Text += String.Format("Запланированая позиция: {0}{1}", Strategy.PositionToString(sState.FuturePos), Environment.NewLine);
            richTextBox1.Text += String.Format("Остановка на следующем сигнале: {0}{1}", sState.FutureStop, Environment.NewLine);

            richTextBox1.Text += String.Format("Запущен ли таймер на остановку стратегии: {0}{1}", sState.isTimerRun, Environment.NewLine);
            richTextBox1.Text += String.Format("Время последнего обновления: {0}{1}", sState.InterrupTime, Environment.NewLine);
        }

        public Information(StrategyState sState, int CurAmount) //Tool tool,
        {
            InitializeComponent();
            ShowState(sState);
            richTextBox1.Text += String.Format("Текущее количество для сделки: {0}{1}", CurAmount, Environment.NewLine);

            //if (tool != null)
            //{
            //    textBoxClassCode.Text = tool.ClassCode; //ClassCode
            //    textBoxClientCode.Text = tool.ClientCode; //ClientCode
            //    textBoxAccountID.Text = tool.AccountID; //AccountID
            //    textBoxFirmID.Text = tool.FirmID; //FirmID
            //    textBoxShortName.Text = tool.Name; //ShortName
            //    textBoxLot.Text = Convert.ToString(tool.Lot); //Lot
            //    textBoxStep.Text = Convert.ToString(tool.Step); //Шаг цены
            //    textBoxGuaranteeProviding.Text = Convert.ToString(tool.GuaranteeProviding); //ГО

            //    string balance = tool.GetBalance();
            //    if (balance != null)
            //        richTextBox1.Text += "Баланс: " + balance + Environment.NewLine;
            //}

            richTextBox1.Text += String.Format("Количество в позиции: {0}{1}", sState.QtyInPos, Environment.NewLine);

            richTextBox1.Text += String.Format("PrevMAShort: {0}{1}", sState.PrevMAShort, Environment.NewLine);
            richTextBox1.Text += String.Format("PrevMALong: {0}{1}", sState.PrevMALong, Environment.NewLine);
            richTextBox1.Text += String.Format("LastMAShort: {0}{1}", sState.LastMAShort, Environment.NewLine);
            richTextBox1.Text += String.Format("LastMALong: {0}{1}{2}", sState.LastMALong, Environment.NewLine, Environment.NewLine);

            richTextBox1.Text += String.Format("IsStartegyRun: {0}{1}", sState.IsStartegyRun, Environment.NewLine);
            richTextBox1.Text += String.Format("Зафиксированное пересечение: {0}{1}", Strategy.PositionToString(sState.PrevCross), Environment.NewLine);
            richTextBox1.Text += String.Format("Текущая позиция: {0}{1}", Strategy.PositionToString(sState.posDirection), Environment.NewLine);
            richTextBox1.Text += String.Format("Запланированая позиция: {0}{1}", Strategy.PositionToString(sState.FuturePos), Environment.NewLine);
            richTextBox1.Text += String.Format("Остановка на следующем сигнале: {0}{1}", sState.FutureStop, Environment.NewLine);
        }

        public Information(StrategyState sState, int CurAmount, Tool tool,
            ExTool.TrailingStopOrder TStop1, ExTool.StopOrder SimpleStop1)
        {
            InitializeComponent();
            ShowState(sState);
            richTextBox1.Text += String.Format("Текущее количество для сделки: {0}{1}", CurAmount, Environment.NewLine);

            if (tool != null)
            {
                textBoxClassCode.Text = tool.ClassCode; //ClassCode
                textBoxClientCode.Text = tool.ClientCode; //ClientCode
                textBoxAccountID.Text = tool.AccountID; //AccountID
                textBoxFirmID.Text = tool.FirmID; //FirmID
                textBoxShortName.Text = tool.Name; //ShortName
                textBoxLot.Text = Convert.ToString(tool.Lot); //Lot
                textBoxStep.Text = Convert.ToString(tool.Step); //Шаг цены
                textBoxGuaranteeProviding.Text = Convert.ToString(tool.GuaranteeProviding); //ГО

                string balance = tool.GetBalance();
                if (balance != null)
                    richTextBox1.Text += "Баланс: " + balance + Environment.NewLine;
            }

            if (TStop1 == null)
            {
                richTextBox1.Text += "Плавающий стоп-ордер не выставлено." + Environment.NewLine;
            }
            else
            {
                richTextBox1.Text += "Информация про плавающий стоп-ордер: " + TStop1.ShowInfo() + Environment.NewLine;
            }

            if (SimpleStop1 == null)
            {
                richTextBox1.Text += "Простой стоп-ордер не выставлено." + Environment.NewLine;
            }
            else
            {
                richTextBox1.Text += "Информация про простой стоп-ордер: " + SimpleStop1.ShowInfo() + Environment.NewLine;
            }
        }
    }
}
