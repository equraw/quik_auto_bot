﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IniParser;
using IniParser.Model;
using System.IO;
using System.Security.Cryptography;

namespace QuikAutoBot
{
    static class IniTool
    {
        static public InvokePrint Print;
        private static Object LockWrite = new Object();

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public static string ConvertName(string Name)
        {
            string SectName = "";
            using (MD5 md5Hash = MD5.Create())
            {
                SectName = "Strategy_" + GetMd5Hash(md5Hash, Name);
            }
            return SectName;
        }

        /// <summary>
        /// Изменение переданных параметров в файле Settings.ini
        /// </summary>
        /// <param name="data"></param>
        public static void ChangeValue(string IniFname, string Section, Dictionary<string, string> data)
        {
            lock (LockWrite)
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData parsedData = fileIniData.ReadFile(IniFname);
                if (!parsedData.Sections.ContainsSection(Section))
                {
                    parsedData.Sections.AddSection(Section);
                }
                SectionData section = parsedData.Sections.GetSectionData(Section);
                foreach (var t in data)
                {
                    try
                    {
                        if (section.Keys.ContainsKey(t.Key))
                            section.Keys.GetKeyData(t.Key).Value = t.Value;
                        else
                            section.Keys.AddKey(t.Key, t.Value);
                    }
                    catch (Exception ex)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print(String.Format("Ошибка изменения ключа {0} в секции {1}: {2}", t.Key, Section, ex.Message), true);
                    }
                }
                fileIniData.WriteFile(IniFname, parsedData);
            }
        }

        /// <summary>
        /// Создает новый файл с переданной информацией
        /// </summary>
        /// <param name="IniFname"></param>
        /// <param name="Section"></param>
        /// <param name="data"></param>
        static public void CreateIniFile(string IniFname, string Section, Dictionary<string, string> data)
        {
            lock (LockWrite)
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData newParsedData = new IniData();
                newParsedData.Sections.AddSection(Section);
                foreach (var t in data)
                {
                    newParsedData.Sections.GetSectionData(Section).Keys.AddKey(t.Key, t.Value);
                }
                fileIniData.WriteFile(IniFname, newParsedData);
            }
        }

        /// <summary>
        /// Считывает настройки с ini файла по указанной Section
        /// </summary>
        static public Dictionary<string, string> ReadSection(string IniFname, string Section)
        {
            lock (LockWrite)
            {
                FileIniDataParser fileIniData = new FileIniDataParser();
                IniData parsedData = fileIniData.ReadFile(IniFname);
                if (!parsedData.Sections.ContainsSection(Section))
                {
                    return null;
                }
                SectionData sdate = parsedData.Sections.GetSectionData(Section);
                Dictionary<string, string> ResDict = new Dictionary<string, string>();
                for (int i = 0; i < sdate.Keys.Count; i++)
                {
                    ResDict.Add(sdate.Keys.ElementAt(i).KeyName, sdate.Keys.ElementAt(i).Value);
                }
                return ResDict;
            }
        }
    }
}
