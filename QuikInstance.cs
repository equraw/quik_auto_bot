﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuikSharp;
using System.Threading;
using System.Windows.Forms;

namespace QuikAutoBot
{
    public static class QuikInstance
    {
        public static InvokePrint Print; // нужно присваивать
        private static bool IsConnect = false;

        private static readonly Lazy<Quik> quikHolder =
        new Lazy<Quik>(() =>
        {
            Print("Подключаемся к терминалу Quik...", true);
            Quik _quik = new Quik(Quik.DefaultPort, new InMemoryStorage());
            _quik.Events.OnClose += OnCloseDo;
            //_quik.Events.OnInit += OnConnected;
            return _quik;
        });

        public static Quik quik
        {
            get
            {
                if (IsConnect == false)
                {
                    if (Connect(quikHolder.Value) == false)
                    {
                        Print("Не удалось подключться к QUIK.", true);
                        return null;
                    }
                }
                return quikHolder.Value;
            }
        }

        /// <summary>
        /// Проверка доступности серверу и при необходимости попытка подключения
        /// </summary>
        /// <returns></returns>
        private static bool Connect(Quik _quik)
        {
            try
            {
                bool isServerConnected = Form1.ExecuteQuery(_quik.Service.IsConnected(), 30000);
                if (isServerConnected)
                {
                    IsConnect = true;
                    return true;
                }
                else
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Соединение с сервером НЕ установлено." + Environment.NewLine);
                    IsConnect = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Неудачная попытка установить соединения с сервером: " + ex.Message + Environment.NewLine, true);
                IsConnect = false;
                return false;
            }
        }

        //private static void OnConnected(int port)
        //{
        //    MessageBox.Show("on connected! port: " + port);
        //}

        /// <summary>
        /// Функция вызывается перед закрытием терминала QUIK.
        /// </summary>
        private static void OnCloseDo()
        {
            IsConnect = false;
            Print("Терминал QUIK был закрыт!", true);
            int bk = 1;
            do
            {
                System.Media.SystemSounds.Beep.Play();
                Thread.Sleep(1000);
            } while (bk++ < 10);

            System.Threading.Timer AlertTimer = new System.Threading.Timer((x) => System.Media.SystemSounds.Beep.Play());
            AlertTimer.Change(0, 1000);
            while (true)
            {
                try
                {
                    if (Form1.ExecuteQuery(quikHolder.Value.Service.IsConnected(), 1000))
                    {
                        IsConnect = true;
                        Print("Соединение с сервером восстановлено!", true);
                        AlertTimer.Dispose();
                        return;
                    }
                }
                catch
                {

                }
            }
        }
    }
}
