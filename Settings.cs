﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuikAutoBot
{
    public partial class Settings : Form
    {
        Form1.FormSettings GFSettings;
        Form1.FormSettings LFSettings;

        public Settings(Form1.FormSettings settings)
        {
            InitializeComponent();
            this.GFSettings = settings;
            ShowSettings(GFSettings);
        }

        private void ShowSettings(Form1.FormSettings FSettings)
        {
            textBox1.Text = FSettings.FreqRestart.ToString();
            textBox3.Text = FSettings.FreqRestartM.ToString();
            textBox2.Text = FSettings.FreqUpdate.ToString();
        }

        private bool ReadParams()
        {
            LFSettings = new Form1.FormSettings();
            try
            {
                LFSettings.FreqRestart = Convert.ToInt32(textBox1.Text);
                if (LFSettings.FreqRestart < 0 || LFSettings.FreqRestart > 24)
                    return false;

                LFSettings.FreqRestartM = Convert.ToInt32(textBox3.Text);
                if (LFSettings.FreqRestartM < 0 || LFSettings.FreqRestartM > 60)
                    return false;

                LFSettings.FreqUpdate = Convert.ToInt32(textBox2.Text);
                if (LFSettings.FreqUpdate < 1)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                MessageBox.Show(ex.Message, "Ошибка");
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ReadParams())
            {
                GFSettings.FreqRestart = LFSettings.FreqRestart;
                GFSettings.FreqRestartM = LFSettings.FreqRestartM;
                GFSettings.FreqUpdate = LFSettings.FreqUpdate;

                (this.Owner as Form1).UpdateSettings(GFSettings);
                this.Close();
            }
            else
            {
                MessageBox.Show("Параметры не сохранены.");
            }
        }
    }
}
