﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using QuikSharp;
using QuikSharp.DataStructures;
using QuikSharp.DataStructures.Transaction;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Globalization;

namespace QuikAutoBot
{
    public class Strategy
    {
        /// <summary>
        /// Ордер для исполнения
        /// </summary>
        private struct ReqOrder
        {
            public Operation Direction { get; set; } //направление
            public int Amount { get; set; } //количество в лотах
            public int BecomePos { get; set; } //измененние в позиции стратегии которое будет после выполнения этого ордера
            public string Comment { get; set; } //комментарий
        }

        public bool IsTimerRun
        {
            get { return State.isTimerRun; }
            set
            {
                State.isTimerRun = value;
                if (value == true)
                {
                    IniTool.ChangeValue(Form1.IniFname, Param.StrategyName, new Dictionary<string, string> {
                        { "SetTime", Param.SetTime.ToString() },
                        { "IsTimerRun", value.ToString() }
                    });
                }
                else
                {
                    IniTool.ChangeValue(Form1.IniFname, Param.StrategyName, new Dictionary<string, string> {
                        { "IsTimerRun", value.ToString() }
                    });
                }
            }
        } //запущен ли таймер остановки

        public int PosDirection
        {
            get { return State.posDirection; }
            private set
            {
                State.posDirection = value;
                OnChangePosition?.Invoke(State.posDirection);
            }
        } //направлени  позиции

        private bool LockUpdate; //блок на выполнение метода Update
        private bool LockPerform; //блок на смену позиции

        public StrategyParam Param; // параметры
        public StrategyState State; //информация про состояние
        public Tool Tool { get; set; } //инструмент
        private SupportTimer Stimer; //поддержка работы таймера остановки
        private InvokePrint Print { get; set; }

        public event Action<int> OnChangePosition; //параметр int - новая позиция
        public event Action<bool, bool> OnChangeState; //параметры: bool FromRun(or Stop), bool Result (good or bad) 
        public event Action<decimal, decimal> OnChangeIndictors; //смена значений индикатора
        public event Action OnConnectTool;

        public System.Threading.Timer timerSaveState; //сохранение состояния
        public System.Threading.Timer timerUpdate; //обновление индикаторов

        public int CurBalancePerc //текущая сумма для ордера с учетом процента
        {
            get
            {
                double balance = Convert.ToDouble(Tool.GetBalance().Replace(",", "."), CultureInfo.InvariantCulture);
                double reqbal = (balance * Param.BalancePerc) / 100;
                double GO = Tool.GuaranteeProviding;
                if (GO == 0)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("ГО = 0, сумма на ордер 1", true);
                    return 1;
                }
                int amount = (int)Math.Floor(reqbal / GO);
                double sec1 = Tool.GetSecurity1();
                if (sec1 != 0 && sec1 < GO)
                {
                    Print("План.чист.поз. + Вар. маржа < ГО, размер позиции уменьшено на 1", true);
                    if (amount > 1) amount -= 1;
                }
                return amount;
            }
        }

        public Strategy(InvokePrint Print, StrategyParam Parameters, StrategyState State)
        {
            try
            {
                this.Print = Print;
                this.Param = Parameters;
                this.State = State;
                this.Tool = null;

                PosDirection = State.posDirection;
                Stimer = new SupportTimer(this.Print);
                Stimer.OnTimerCallbackBegin += TimerCallbackBegin;
                Stimer.OnTimerCallbackStop += TimerCallbackStop;
                if (State.isTimerRun)
                {
                    IsTimerRun = Stimer.Start(Param.SetTime);
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка создания стратегии: " + ex.Message, true);
            }
        }

        private void OnChangePrices(decimal bid, decimal offer)
        {
            if (State.SimpleStop1 != null)
            {
                bool CheckResult = false;
                if (State.SimpleStop1.DirectionBuy)
                    CheckResult = State.SimpleStop1.UpdatePrice(offer);
                else
                    CheckResult = State.SimpleStop1.UpdatePrice(bid);

                if (CheckResult)
                {
                    int npos = State.SimpleStop1.DirectionBuy ? 1 : -1;
                    ExposeOrder(npos, true);
                }
            }

            if (State.TStop1 != null)
            {
                bool CheckResult = false;
                if (State.TStop1.DirectionBuy)
                    CheckResult = State.TStop1.UpdatePrice(offer);
                else
                    CheckResult = State.TStop1.UpdatePrice(bid);

                if (CheckResult)
                {
                    ExposeOrder(0, true);
                }
            }
        }

        /// <summary>
        /// Пересоздание объекта Tool
        /// </summary>
        /// <returns></returns>
        private bool ConnectTool()
        {
            try
            {
                this.Tool = new Tool(Param.SecCode, Print);
                if (Tool.SuccessfulConnect)
                {
                    Print("Инструмент " + Tool.Name + " создан.");
                    OnConnectTool?.Invoke();
                    return true;
                }
                else
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Неправильная инициализация tool.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка инициализации tool: " + ex.Message);
                return false;
            }
        }

        public void SaveState()
        {
            try
            {
                string SectName = IniTool.ConvertName("Strategy_" + Param.StrategyName);
                StrategyParam.SaveParam(Param, SectName);
                StrategyState.SaveState(State, SectName);
                //Print("Состояние стратегии сохранено в файл.", true);
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Произошла ошибка при сохранении стратегии: " + ex.Message);
            }
        }

        private void TimerSave(object state)
        {
            SaveState();
        }

        /// <summary>
        /// Запуск бота
        /// </summary>
        /// <param name="shortTag"></param>
        /// <param name="longTag"></param>
        /// <param name="diffPerc"></param>
        /// <param name="BalPerc"></param>
        /// <param name="ptool"></param>
        public void Start(int UpdateSec, StrategyParam? Param = null)
        {
            if (Tool == null || Tool.SuccessfulConnect == false)
            {
                if (!ConnectTool())
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Не удалось инициилизировать Tool, запуск стратегии прервано.", true);
                    return;
                }
            }

            if (State.IsStartegyRun == false)
            {
                try
                {
                    Tool.OnNewPrices += OnChangePrices;
                    if (Param != null) //чистый запуск
                    {
                        this.Param = (StrategyParam)Param;
                        State = new StrategyState();
                        decimal[] MAValues = GetLastMA();
                        if (MAValues == null)
                        {
                            OnChangeState?.Invoke(true, false);
                            return;
                        }
                        State.LastMAShort = MAValues[0];
                        State.LastMALong = MAValues[1];
                        PosDirection = 0;
                    }
                    else
                    {
                        if (DateTime.Now - State.InterrupTime > TimeSpan.FromMinutes(10))
                        {
                            State.PrevMAShort = 0;
                            State.PrevMALong = 0;
                            Print("Времени после прерывания прошло больше 10 мин. Последние значения индикаторов не загружаются.", true);
                        }
                        if (State.LastMAShort == 0 || State.LastMALong == 0)
                        {
                            decimal[] MAValues = GetLastMA();
                            if (MAValues == null)
                            {
                                OnChangeState?.Invoke(true, false);
                                return;
                            }
                            State.LastMAShort = MAValues[0];
                            State.LastMALong = MAValues[1];
                        }
                    }
                    State.IsStartegyRun = true;

                    if (timerSaveState != null)
                    {
                        timerSaveState.Dispose();
                    }
                    timerSaveState = new System.Threading.Timer(TimerSave);
                    timerSaveState.Change(TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(1));

                    OnChangeIndictors?.Invoke(State.LastMAShort, State.LastMALong);
                    OnChangeState?.Invoke(true, true);

                    if (timerUpdate != null)
                    {
                        timerUpdate.Dispose();
                    }
                    timerUpdate = new System.Threading.Timer(TimerUpdate);
                    timerUpdate.Change(TimeSpan.FromSeconds(UpdateSec), TimeSpan.FromSeconds(UpdateSec));
                }
                catch (Exception ex)
                {
                    OnChangeState?.Invoke(true, false);
                    System.Media.SystemSounds.Beep.Play();
                    Print("Ошибка при запуске стратегии: " + ex.Message, true);
                }
            }
        }

        /// <summary>
        /// Остановка стратегии
        /// </summary>
        public void Stop()
        {
            while (LockUpdate == true)
            {
                Thread.Sleep(100);
            }
            LockUpdate = true;
            try
            {
                Tool.OnNewPrices -= OnChangePrices;
                State.TStop1 = null;
                State.SimpleStop1 = null;
                if (State.IsStartegyRun == true && PosDirection != 0)
                {
                    List<ReqOrder> dplan = new List<ReqOrder>();
                    if (PosDirection == 1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Sell,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = -1,
                            Comment = "ордер закрытие позиции лонг"
                        });
                    }
                    if (PosDirection == -1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Buy,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = 1,
                            Comment = "ордер закрытие позиции шорт"
                        });
                    }
                    if (dplan.Count > 0)
                    {
                        bool resImplem = ImplementOrders(dplan);
                        if (resImplem == true)
                        {
                            Print("Закрытие позиции выполнено успешно.", true);
                        }
                        else
                        {
                            System.Media.SystemSounds.Beep.Play();
                            Print("Произошла ошибка при закрытии позиции.", true);
                            OnChangeState?.Invoke(false, false);
                            return;
                        }
                    }
                }
                State.IsStartegyRun = false;
                OnChangeState?.Invoke(false, true);
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка при остановке стратегии: " + ex.Message, true);
                OnChangeState?.Invoke(false, false);
            }
            timerSaveState.Change(0, Timeout.Infinite);
            if (timerSaveState != null)
            {
                timerSaveState.Dispose();
            }
            if (timerUpdate != null)
            {
                timerUpdate.Dispose();
            }
            LockUpdate = false;
        }

        /// <summary>
        /// Обновление индикаторов и принятие решения
        /// </summary>
        public void Update()
        {
            if (LockUpdate) return;
            LockUpdate = true;
            try
            {
                decimal[] MAValues = GetLastMA();
                if (MAValues == null)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Индикаторы стратегии не обновлено.");
                    return;
                }
                if (State.LastMAShort != 0)
                {
                    State.PrevMAShort = State.LastMAShort;
                }
                else
                {
                    //System.Media.SystemSounds.Beep.Play();
                    //Print("Индикаторы стратегии не обновлено.");
                    //return;
                }
                State.LastMAShort = MAValues[0];

                if (State.LastMALong != 0)
                {
                    State.PrevMALong = State.LastMALong;
                }
                else
                {
                    //System.Media.SystemSounds.Beep.Play();
                    //Print("Индикаторы стратегии не обновлено.");
                    //return;
                }
                State.LastMALong = MAValues[1];
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка при обновлении индикаторов: " + ex.Message, true);
                LockUpdate = false;
                return;
            }
            OnChangeIndictors?.Invoke(State.LastMAShort, State.LastMALong);
            Decide();
            LockUpdate = false;
        }

        /// <summary>
        /// Возвращает направление пересечения
        /// </summary>
        /// <returns></returns>
        private int IsCrosses()
        {
            int result = 0;
            if (State.PrevMALong > State.PrevMAShort && State.LastMAShort > State.LastMALong)
            {
                result = 1;
            }
            else
            {
                if (State.PrevMALong < State.PrevMAShort && State.LastMAShort < State.LastMALong)
                    result = -1;
            }
            return result;
        }

        /// <summary>
        /// Смена позиции стратегии
        /// </summary>
        /// <param name="NState">Позиция на которую нужно сменить</param>
        /// <param name="WithImplem">С выставлением ордеров или без</param>
        /// <param name="WithStop">Выполнить на след. сигнале и остановиться</param>
        public void ChangeState(int NState, bool WithImplem, bool WithStop, bool FromSignal = false)
        {
            while (LockPerform)
            {
                Thread.Sleep(100);
            }
            LockPerform = true;

            if (FromSignal == false) //вызвалось НЕ при срабатывании сигнала (с form1)
            {
                if (WithImplem == true) //нужно выставлять ордер
                {
                    if (WithStop == true) //выполнить не сейчас, а при сраб. сигнала
                    {
                        State.FuturePos = NState; //требуемая позиция
                        State.FutureStop = true; //флаг остановки 
                    }
                    else //выполнить сейчас
                    {
                        State.FutureStop = false; //отменить флаг остановки
                        ExposeOrder(NState);//ставим ордера
                    }
                }
                else //смена позиции без ордеров
                {
                    ExposeOrder(NState, false);
                    //PosDirection = NState;
                }
            }
            else //вызвалось на сигнале
            {
                if (State.FutureStop == true) //есть флаг остановки
                {
                    ExposeOrder(State.FuturePos);//меняем на указанную
                    Stop(); //останавливаем стратегию
                    StrategyAutoStop();
                }
                else
                {
                    ExposeOrder(NState); //действия по сигналу
                }
            }
            LockPerform = false;
        }

        /// <summary>
        /// Запланировать какие ордера выставлять в завис. от текущ. позиц. и указанного направления
        /// </summary>
        /// <param name="pdir"></param>
        private void ExposeOrder(int NewState, bool WithExpose = true)
        {
            List<ReqOrder> dplan = new List<ReqOrder>();
            if (NewState == 1)
            {
                Print("Сигнал на покупку.", true);
                if (PosDirection == 1)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("С позиции лонг еще не вышли.");
                }
                else
                {
                    if (PosDirection == -1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Buy,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = 1,
                            Comment = "ордер выхода из шорта"
                        });
                    }
                    dplan.Add(new ReqOrder
                    {
                        Direction = Operation.Buy,
                        Amount = CurBalancePerc,
                        BecomePos = 1,
                        Comment = "ордер входа в лонг"
                    });
                }
            }

            if (NewState == -1)
            {
                Print("Сигнал на продажу.", true);
                if (PosDirection == -1)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("С позиции шорт еще не вышли.");
                }
                else
                {
                    if (PosDirection == 1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Sell,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = -1,
                            Comment = "ордер выхода из лонга"
                        });
                    }
                    dplan.Add(new ReqOrder
                    {
                        Direction = Operation.Sell,
                        Amount = CurBalancePerc,
                        BecomePos = -1,
                        Comment = "ордер входа в шорт"
                    });
                }
            }

            if (NewState == 0)
            {
                Print("Закрытие позиции.", true);
                if (PosDirection == 0)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Позиция не открыта.");
                }
                else
                {
                    if (PosDirection == 1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Sell,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = -1,
                            Comment = "ордер выхода из лонга"
                        });
                    }
                    if (PosDirection == -1)
                    {
                        dplan.Add(new ReqOrder
                        {
                            Direction = Operation.Buy,
                            Amount = Math.Abs(State.QtyInPos),
                            BecomePos = 1,
                            Comment = "ордер выхода из шорта"
                        });
                    }
                }
            }

            if (dplan.Count > 0)
            {
                State.TStop1 = null;
                State.SimpleStop1 = null;
                ImplementOrders(dplan, WithExpose);

                if (PosDirection == 1)
                {
                    double BidPrice = (double)Tool.bid;
                    if (BidPrice == 0)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Ошибка выставления стоп-ордера: цена bid не получена!", true);
                        return;
                    }

                    if (Param.SetTrailingStop)
                    {
                        decimal StopPrice = (decimal)(BidPrice - BidPrice * (Param.TStopDiff / 100d));
                        State.TStop1 = new ExTool.TrailingStopOrder(false, dplan.Last().Amount, StopPrice, (decimal)BidPrice);
                    }

                    if (Param.SetSimpleStop)
                    {
                        decimal SSPrice = (decimal)(BidPrice - Param.SimpleStopValue);
                        State.SimpleStop1 = new ExTool.StopOrder(false, dplan.Last().Amount, SSPrice);
                    }
                }

                if (PosDirection == -1)
                {
                    double AskPrice = (double)Tool.offer;
                    if (AskPrice == 0)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Ошибка выставления стоп-ордера: цена ask не получена!", true);
                        return;
                    }

                    if (Param.SetTrailingStop)
                    {
                        decimal StopPrice = (decimal)(AskPrice + AskPrice * (Param.TStopDiff / 100d));
                        State.TStop1 = new ExTool.TrailingStopOrder(true, dplan.Last().Amount, StopPrice, (decimal)AskPrice);
                    }

                    if (Param.SetSimpleStop)
                    {
                        decimal SSPrice = (decimal)(AskPrice + Param.SimpleStopValue);
                        State.SimpleStop1 = new ExTool.StopOrder(true, dplan.Last().Amount, SSPrice);
                    }
                }
            }
        }

        /// <summary>
        /// Выставить запланированные ордера на биржу
        /// </summary>
        private bool ImplementOrders(List<ReqOrder> Orders, bool WithExpose = true)
        {
            bool result = true;
            for (int i = 0; i < Orders.Count; i++)
            {
                if (WithExpose == false)
                {
                    PosDirection += Orders[i].BecomePos;
                    State.QtyInPos += Orders[i].BecomePos * Orders[i].Amount;
                    continue;
                }
                try
                {
                    decimal priceInOrder = 0;
                    if (Orders[i].Direction == Operation.Buy)
                    {
                        priceInOrder = Math.Round(Tool.offer + Tool.Step * 30, Tool.PriceAccuracy);
                    }
                    else
                    {
                        priceInOrder = Math.Round(Tool.bid - Tool.Step * 30, Tool.PriceAccuracy);
                        if (priceInOrder < 0)
                        {
                            priceInOrder = Tool.Step;
                        }
                    }
                    Print(String.Format("[{0}]: сумма - {1}, цена - {2}, направление - {3}", Orders[i].Comment, Orders[i].Amount, priceInOrder,
                        Orders[i].Direction == Operation.Buy ? "buy" : "sell"));

                    Order resOrder = Tool.SubmitOrder(Orders[i].Direction, priceInOrder, Orders[i].Amount);
                    if (resOrder != null)
                    {
                        PosDirection += Orders[i].BecomePos;
                        State.QtyInPos += Orders[i].BecomePos * Orders[i].Amount;
                        Print("[" + Orders[i].Comment + "]" + " исполнен успешно.", true);
                    }
                    else
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Произошла ошибка при исполнении [" + Orders[i].Comment + "].", true);
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Ошибка выставления ордера: " + ex.Message, true);
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// Принятие решения на смену позиции
        /// </summary>
        private void Decide()
        {
            double pdiff = 0;
            int CurCross = 0;
            try
            {
                pdiff = 100 - (double)(Math.Min(State.LastMALong, State.LastMAShort) / Math.Max(State.LastMALong, State.LastMAShort)) * 100;
                CurCross = IsCrosses();
            }
            catch
            {
                return;
            }
            if (pdiff >= Param.DiffPerc)   //разница достаточна
            {
                //Print("Текущая разница: " + Math.Round(pdiff, 3) + " достаточна");
                if (CurCross != 0) //пересеч. сейчас
                {
                    Print("Есть пересечение и разница достаточна - смена позиции", true);
                    ChangeState(CurCross, true, false, true);//ExposeOrder(CurCross);
                }
                else
                {
                    if (State.PrevCross != 0) //было пересеч. раньше
                    {
                        Print("Разница достаточна, смена позиции по прошлому пересечению", true);
                        ChangeState(State.PrevCross, true, false, true);//ExposeOrder(PrevCross);
                    }
                }
                State.PrevCross = 0; //аннулировать пересеч. раньше
            }
            else
            {
                //Print("Текущая разница: " + Math.Round(pdiff, 3) + " не достаточна");
                if (CurCross != 0)
                {
                    Print("Разница не достаточна, запомнить текущее пересечение", true);
                    State.PrevCross = CurCross; //запомнить пересечение
                }
            }
        }

        /// <summary>
        /// Получает значения 2 индикаторов MA из quik по их меткам
        /// </summary>
        /// <returns>Сначала значение Short, потом Long</returns>
        private decimal[] GetLastMA()
        {
            if (Param.ShortTag == "" || Param.LongTag == "")
            {
                return null;
            }
            List<Candle> ShortVal = Form1.ExecuteQuery(QuikInstance.quik.ExactFunc.GetLastIndicator(Param.ShortTag));
            //if (ShortVal.Count == 0)
            //{
            //    return null;
            //}
            List<Candle> LongVal = Form1.ExecuteQuery(QuikInstance.quik.ExactFunc.GetLastIndicator(Param.LongTag));
            //Print("Short: " + ShortVal.Last().Close);
            //Print("Long: " + LongVal[0].Close);
            return new decimal[] {
                ShortVal.Count == 0 ? 0 :ShortVal[0].Close,
                LongVal.Count == 0 ? 0 : LongVal[0].Close };
        }

        static public string PositionToString(int value)
        {
            string Result = "None";
            if (value == -1)
            {
                Result = "Short";
            }
            if (value == 1)
            {
                Result = "Long";
            }
            return Result;
        }

        //Регулярный перерасчет индикаторов стратегии
        private void TimerUpdate(object state)
        {
            try
            {
                decimal bk1 = Tool.LastPrice;
            }
            catch
            {
                return;
            }
            if (State.IsStartegyRun)
            {
                try
                {
                    ThreadPool.QueueUserWorkItem((x) => Update());
                }
                catch (Exception ex)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Ошибка при обновлении индикаторов стратегии: " + ex.Message, true);
                }
            }
        }

        /// <summary>
        /// Срабатывание предварительной остановки
        /// </summary>
        /// <param name="state"></param>
        private void TimerCallbackBegin(object state)
        {
            ChangeState(0, true, true);
            System.Media.SystemSounds.Beep.Play();
            Print("Сработал таймер предварительного закрытия до даты погашения: ", true);
        }

        /// <summary>
        /// Срабатывание рехкой остановки
        /// </summary>
        /// <param name="state"></param>
        private void TimerCallbackStop(object state)
        {
            IsTimerRun = false;
            Stop();
            System.Media.SystemSounds.Beep.Play();
            Print("Сработал таймер резкого закрытия до даты погашения: ", true);
        }

        /// <summary>
        /// Установить таймер на предварительную и резкую остановку
        /// </summary>
        /// <param name="time"></param>
        public void SetTimerStop(DateTime time)
        {
            if (Stimer.timerBeg != null)
            {
                Stimer.timerBeg.Dispose();
            }
            if (Stimer.timerStop != null)
            {
                Stimer.timerStop.Dispose();
            }
            Param.SetTime = time;
            IsTimerRun = Stimer.Start(Param.SetTime);
        }

        /// <summary>
        /// Вызывается при остановке по флагу
        /// </summary>
        private void StrategyAutoStop()
        {
            if (Stimer.timerBeg != null)
            {
                Stimer.timerBeg.Dispose();
            }
            if (Stimer.timerStop != null)
            {
                Stimer.timerStop.Dispose();
            }
            IsTimerRun = false;
        }
    }
}
