﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Globalization;
using System.Threading;

namespace QuikAutoBot
{
    public class SupportTimer
    {
        public System.Threading.Timer timerBeg;
        public System.Threading.Timer timerStop;
        private System.Threading.Timer timerLater;
        private DateTime DTime;

        public event TimerCallback OnTimerCallbackBegin;
        public event TimerCallback OnTimerCallbackStop;
        private InvokePrint Print { get; set; }

        public SupportTimer(InvokePrint Print)
        {
            this.Print = Print;
        }

        public bool Start(DateTime pDTime)
        {
            this.DTime = pDTime;
            DateTime now = DateTime.Now;
            if (DTime < now)
            {
                Print("Указано прошедшее время.", true);
                return false;
            }
            OnLater(null);
            Print("Таймер остановки бота запущенно на " + pDTime.ToString(), true);
            return true;
        }

        /// <summary>
        /// Проверка на возможность запустить прямые таймера
        /// </summary>
        /// <param name="state"></param>
        private void OnLater(object state)
        {
            if (timerLater != null)
            {
                timerLater.Dispose();
            }
            DateTime now = DateTime.Now;
            if (DTime - now <= new TimeSpan(30, 0, 0, 0))
            {
                SetUpTimers(DTime); //прямой запуск
            }
            else
            {
                timerLater = new System.Threading.Timer(OnLater);
                timerLater.Change(new TimeSpan(29, 0, 0, 0), new TimeSpan(-1)); //отложить
            }
        }

        /// <summary>
        /// Прямой запуск 2 таймеров
        /// </summary>
        /// <param name="MatDate"></param>
        private void SetUpTimers(DateTime MatDate)
        {
            if (timerBeg != null)
            {
                timerBeg.Dispose();
            }
            if (timerStop != null)
            {
                timerStop.Dispose();
            }
            DateTime BegMatDate = new DateTime(MatDate.Year, MatDate.Month, MatDate.Day);
            BegMatDate = BegMatDate.AddMinutes(1);
            DateTime now = DateTime.Now;
            if (now < BegMatDate)
            {
                timerBeg = new System.Threading.Timer(OnTimerCallbackBegin);
                long msUntilFour = (long)((BegMatDate - now).TotalMilliseconds);
                timerBeg.Change(msUntilFour, Timeout.Infinite);
                Print("Предварительный таймер запущено на " + BegMatDate.ToString(), true);
            }
            if (now < MatDate)
            {
                timerStop = new System.Threading.Timer(OnTimerCallbackStop);
                long msUntilFour = (long)((MatDate - now).TotalMilliseconds);
                timerStop.Change(msUntilFour, Timeout.Infinite);
                Print("Таймер резкой остановки запущено на " + MatDate.ToString());
            }
        }
    }
}
