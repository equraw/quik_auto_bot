﻿using System;
using QuikSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuikSharp.DataStructures;
using QuikSharp.DataStructures.Transaction;
using System.Threading;
using System.Globalization;

namespace QuikAutoBot
{
    public class Tool
    {
        //private Char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
        private decimal lastPrice;
        private InvokePrint Print;

        #region Свойства
        /// <summary>
        /// Краткое наименование инструмента (бумаги)
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Код инструмента (бумаги)
        /// </summary>
        public string SecurityCode { get; private set; }
        /// <summary>
        /// Код класса инструмента (бумаги)
        /// </summary>
        public string ClassCode { get; private set; }
        /// <summary>
        /// Счет клиента
        /// </summary>
        public string AccountID { get; private set; }
        /// <summary>
        /// Код фирмы
        /// </summary>
        public string FirmID { get; private set; }
        /// <summary>
        /// Количество акций в одном лоте
        /// Для инструментов класса SPBFUT = 1
        /// </summary>
        public int Lot { get; private set; }
        /// <summary>
        /// Точность цены (количество знаков после запятой)
        /// </summary>
        public int PriceAccuracy { get; private set; }
        /// <summary>
        /// Шаг цены
        /// </summary>
        public decimal Step { get; private set; }
        /// <summary>
        /// Гарантийное обеспечение (только для срочного рынка)
        /// для фондовой секции = 0
        /// </summary>
        public double GuaranteeProviding
        {
            get
            {
                try
                {
                    ParamTable pTable = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "BUYDEPO"));
                    return Convert.ToDouble(pTable.ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    throw new Exception("Не удалось получить ГО: " + ex.Message);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ClientCode { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        //public int Qty { get; private set; }
        /// <summary>
        /// Цена последней сделки
        /// </summary>
        public decimal LastPrice
        {
            get
            {
                ParamTable ptable = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "LAST"));
                lastPrice = Convert.ToDecimal(ptable.ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture);
                return lastPrice;
            }
        }
        public bool SuccessfulConnect { get; private set; } //успешная инициализация инструмента

        public decimal bid = 0;
        public decimal offer = 0;
        #endregion

        public Action<decimal, decimal> OnNewPrices; //bid, offer
        OrderBook toolOrderBook;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="QuikInstance.quik"></param>
        /// <param name="securityCode">Код инструмента</param>
        /// <param name="classCode">Код класса</param>
        public Tool(string securityCode, InvokePrint print)
        {
            Print = print;
            GetBaseParam(securityCode);
        }

        public void TestCall()
        {
            OnNewPrices?.Invoke(23, 15);
            Print("test called");
        }

        void OnQuoteDo(OrderBook quote)
        {
            try
            {
                if (quote.sec_code == SecurityCode && quote.class_code == ClassCode)
                {
                    if (quote.bid != null && quote.offer != null)
                    {
                        toolOrderBook = quote;
                        bid = Convert.ToDecimal(toolOrderBook.bid[toolOrderBook.bid.Length - 1].price);
                        offer = Convert.ToDecimal(toolOrderBook.offer[0].price);
                        OnNewPrices?.Invoke(bid, offer);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Error in OnQuoteDo: " + ex.Message);
            }
        }

        void OnFuturesClientHoldingDo(FuturesClientHolding futPos)
        {
            //if (futPos.secCode == SecurityCode) futuresPosition = futPos;
            //Print(String.Format("Изменении позиции по срочному рынку. Тип лимита: {0}, Стоимость позиций: {0}",
            //    futPos.type, futPos.positionValue) + Environment.NewLine);
        }

        void OnDepoLimitDo(DepoLimitEx depLimit)
        {
            Print("Вызвано событие OnDepoLimit (изменение бумажного лимита)...");
            Print("Заблокировано на покупку количества лотов: " + depLimit.LockedBuy + Environment.NewLine);
        }

        public bool SubscribeQuote()
        {
            try
            {
                QuikInstance.quik.Events.OnQuote += OnQuoteDo;
                bool isSubscribedToolOrderBook = Form1.ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(ClassCode, SecurityCode));
                if (isSubscribedToolOrderBook == true)
                {
                    return true;
                }
                else
                {
                    Form1.ExecuteQuery(QuikInstance.quik.OrderBook.Subscribe(ClassCode, SecurityCode));
                    isSubscribedToolOrderBook = Form1.ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(ClassCode, SecurityCode));
                    return isSubscribedToolOrderBook;
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка обработки подписи " + Name + ": " + ex.Message);
                return false;
            }
        }

        public void UnsubscribeQuote()
        {
            try
            {
                bool isSubscribedToolOrderBook = Form1.ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(ClassCode, SecurityCode));
                if (isSubscribedToolOrderBook == true)
                {
                    Form1.ExecuteQuery(QuikInstance.quik.OrderBook.Unsubscribe(ClassCode, SecurityCode));
                }
                QuikInstance.quik.Events.OnQuote -= OnQuoteDo;
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка обработки отписки " + Name + ": " + ex.Message);
            }
        }

        private bool Subscribe()
        {
            try
            {
                bool isSubscribedToolOrderBook = Form1.ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(ClassCode, SecurityCode));
                if (isSubscribedToolOrderBook == true)
                {
                    Print("Отписываемся от предыдущего стакана...");
                    Form1.ExecuteQuery(QuikInstance.quik.OrderBook.Unsubscribe(ClassCode, SecurityCode));
                }
                Print("Подписываемся на стакан...");
                Form1.ExecuteQuery(QuikInstance.quik.OrderBook.Subscribe(ClassCode, SecurityCode));
                isSubscribedToolOrderBook = Form1.ExecuteQuery(QuikInstance.quik.OrderBook.IsSubscribed(ClassCode, SecurityCode));

                if (isSubscribedToolOrderBook)
                {
                    Print("Подписка на стакан прошла успешно." + Environment.NewLine);
                    toolOrderBook = new OrderBook();
                    Print("Подписываемся на колбэк 'OnQuote'...");
                    QuikInstance.quik.Events.OnQuote += OnQuoteDo;

                    Print("Подписываемся на колбэк 'OnFuturesClientHolding'...");
                    QuikInstance.quik.Events.OnFuturesClientHolding += OnFuturesClientHoldingDo;
                    Print("Подписываемся на колбэк 'OnDepoLimit'...");
                    QuikInstance.quik.Events.OnDepoLimit += OnDepoLimitDo;
                }
                else
                {
                    Print("Подписка на стакан не удалась." + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка обработки подписок: " + ex.Message);
                return false;
            }
            return true;
        }

        private string GetClassCode(string secCode)
        {
            Print("Определяем код класса инструмента " + secCode + " по списку классов...");
            string classCode = "";
            try
            {
                string[] allclases = Form1.ExecuteQuery(QuikInstance.quik.Class.GetClassesList());
                string sclases = "";
                for (int i = 0; i < allclases.Length; i++)
                {
                    sclases += allclases[i] + " ";
                }
                Print("Список всех возможных классов: " + sclases);
                classCode = Form1.ExecuteQuery(QuikInstance.quik.Class.GetSecurityClass(sclases, secCode));
                Print("Установленный клас текущего инструмента: " + classCode + Environment.NewLine);
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка определения класса инструмента. Убедитесь, что тикер указан правильно. " + ex.Message + Environment.NewLine, true);
            }
            return classCode;
        }

        private void GetBaseParam(string secCode)
        {
            try
            {
                SecurityCode = secCode;
                if (QuikInstance.quik != null)
                {
                    ClassCode = GetClassCode(SecurityCode);
                    if (ClassCode != null && ClassCode != "")
                    {
                        try
                        {
                            Name = (Form1.ExecuteQuery(QuikInstance.quik.Class.GetSecurityInfo(ClassCode, SecurityCode))).ShortName;
                            AccountID = Form1.ExecuteQuery(QuikInstance.quik.Class.GetTradeAccount(ClassCode));
                            FirmID = (Form1.ExecuteQuery(QuikInstance.quik.Class.GetClassInfo(ClassCode))).FirmId;
                            Step = Convert.ToDecimal(
                                (Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "SEC_PRICE_STEP")))
                                .ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture);
                            PriceAccuracy = Convert.ToInt32(Convert.ToDouble(
                                (Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "SEC_SCALE")))
                                .ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture));
                            ClientCode = Form1.ExecuteQuery(QuikInstance.quik.Class.GetClientCode());
                            //Qty = GetPositionT2(ClientCode);
                        }
                        catch (Exception ex)
                        {
                            System.Media.SystemSounds.Beep.Play();
                            Print("Tool.GetBaseParam. Ошибка получения наименования для " + SecurityCode + ": " + ex.Message);
                            SuccessfulConnect = false;
                            return;
                        }

                        if (ClassCode == "SPBFUT")
                        {
                            Lot = 1;
                            var GO = GuaranteeProviding;
                            Print("ГО: " + GO);
                        }
                        else
                        {
                            Lot = Convert.ToInt32(Convert.ToDouble((Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "LOTSIZE")))
                                .ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture));
                            //GuaranteeProviding = 0;
                        }
                        //Subscribe();
                        try
                        {
                            ParamTable ptable = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "BID"));
                            bid = Convert.ToDecimal(ptable.ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture);

                            ptable = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetParamEx(ClassCode, SecurityCode, "OFFER"));
                            offer = Convert.ToDecimal(ptable.ParamValue.Replace(',', '.'), CultureInfo.InvariantCulture);
                        }
                        catch (Exception ex)
                        {
                            System.Media.SystemSounds.Beep.Play();
                            Print("Ошибка получения bid и offer: " + ex.Message);
                        }
                    }
                    else
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Tool.GetBaseParam. Ошибка: classCode не определен.");
                        Lot = 0;
                        //GuaranteeProviding = 0;
                        SuccessfulConnect = false;
                        return;
                    }
                }
                else
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("Tool.GetBaseParam quik = null !");
                    SuccessfulConnect = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка в методе GetBaseParam: " + ex.Message);
                SuccessfulConnect = false;
                return;
            }
            SuccessfulConnect = true;
        }

        private int GetPositionT2(string clientCode)
        {
            // возвращает чистую позицию по инструменту
            // для срочного рынка передаем номер счета, для спот-рынка код-клиента
            int qty = 0;
            if (ClassCode == "SPBFUT")
            {
                // фьючерсы
                try
                {
                    FuturesClientHolding q1 = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetFuturesHolding(FirmID, AccountID, SecurityCode, 0));
                    if (q1 != null) qty = Convert.ToInt32(q1.totalNet);
                }
                catch (Exception ex)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("GetPositionT2: SPBFUT, ошибка - " + ex.Message + Environment.NewLine);
                }
            }
            else
            {
                // акции
                try
                {
                    DepoLimitEx q1 = Form1.ExecuteQuery(QuikInstance.quik.Trading.GetDepoEx(FirmID, clientCode, SecurityCode, AccountID, 2));
                    if (q1 != null) qty = Convert.ToInt32(q1.CurrentBalance);
                }
                catch (Exception ex)
                {
                    System.Media.SystemSounds.Beep.Play();
                    Print("GetPositionT2: ошибка - " + ex.Message + Environment.NewLine);
                }
            }
            return qty;
        }

        private FuturesClientHolding GetFutHolding()
        {
            var task = QuikInstance.quik.Trading.GetFuturesHolding(FirmID, AccountID, SecurityCode, 4);
            if (task.Wait(5000))
            {
                return task.Result;
            }
            else
            {
                return null;
            }
        }

        private DepoLimit getDepoLim()
        {
            var task = QuikInstance.quik.Trading.GetDepo(ClientCode, FirmID, SecurityCode, AccountID);
            if (task.Wait(5000))
            {
                return task.Result;
            }
            else
            {
                return null;
            }
        }

        private MoneyLimit getMoneyLimit()
        {
            var task = QuikInstance.quik.Trading.GetMoney(ClientCode, FirmID, SecurityCode, AccountID);
            if (task.Wait(5000))
            {
                return task.Result;
            }
            else
            {
                return null;
            }
        }

        //Тип лимита limit_type. Возможные значения: 
        //«0» – «Денежные средства»,
        //«1» – «Залоговые денежные средства», 
        //«2» – «Всего», 
        //«3» – «Клиринговые рубли»,
        //«4» – «Клиринговые залоговые рубли», 
        //«5» – «Лимит открытых позиций на спот-рынке» 
        //currcode - Валюта, в которой транслируется ограничение 
        private string getFuturesLimit()
        {
            var task = QuikInstance.quik.Trading.GetFuturesLimit(FirmID, AccountID, 2, SecurityCode);
            if (task.Wait(5000))
            {
                return task.Result;
            }
            else
            {
                return null;
            }
        }

        public double GetSecurity1()
        {
            try
            {
                return Convert.ToDouble(Form1.ExecuteQuery(QuikInstance.quik.ExactFunc.GetSecurity1()).
                    Replace(",", "."), CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Ошибка при получении значения \"План.чист.поз. + Вар. маржа\": " + ex.Message, true);
                return 0;
            }
        }

        public string GetBalance()
        {
            return Form1.ExecuteQuery(QuikInstance.quik.ExactFunc.GetFuturesBalance(), 5000);
        }

        private long NewOrder(Operation operation, decimal price, int qty)
        {
            long res = 0;
            Order order_new = new Order
            {
                ClassCode = ClassCode,
                SecCode = SecurityCode,
                Operation = operation,
                Price = price,
                Quantity = qty,
                Account = AccountID
            };
            try
            {
                res = Form1.ExecuteQuery(QuikInstance.quik.Orders.CreateOrder(order_new));
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Неудачная попытка отправки заявки: " + ex.Message);
            }
            return res;
        }

        public Order SubmitOrder(Operation operation, decimal priceInOrder, int Amount)
        {
            Order order = null;
            long transactionID = 0;
            if (operation == Operation.Buy)
            {
                Print("Выставляем заявку на покупку, по цене:" + priceInOrder + " ...");
                transactionID = NewOrder(operation, priceInOrder, Amount);
            }
            else
            {
                Print("Выставляем заявку на продажу, по цене:" + priceInOrder + " ...");
                transactionID = NewOrder(operation, priceInOrder, Amount);
            }
            if (transactionID > 0)
            {
                Thread.Sleep(500);
                Print("Заявка выставлена. ID транзакции - " + transactionID);
                int curit = 0, bk1 = 50;
                bool FindOrder = false;
                while (curit < bk1 && FindOrder == false)
                {
                    try
                    {
                        List<Order> listOrders = Form1.ExecuteQuery(QuikInstance.quik.Orders.GetOrders());
                        foreach (Order _order in listOrders)
                        {
                            if (_order.TransID == transactionID && _order.ClassCode == ClassCode && _order.SecCode == SecurityCode)
                            {
                                Print("Заявка выставлена. Номер заявки: " + _order.OrderNum);
                                order = _order;
                                FindOrder = true;
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        Print("Ошибка получения номера заявки: " + ex.Message + Environment.NewLine);
                    }
                    curit++;
                    if (curit < bk1 && FindOrder == false)
                        Thread.Sleep(500);
                }
            }
            else
            {
                System.Media.SystemSounds.Beep.Play();
                Print("Неудачная попытка выставления заявки.");
            }
            return order;
        }
    }
}
